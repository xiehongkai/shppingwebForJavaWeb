# 衣锦年华商城javaweb
- 用途说明：所有素材来自百姿服饰天猫店，项目仅供学习，请勿用作商业途径。否则可能承受相应的法律责任。
#### 介绍
大三时出于喜爱，将php的商城项目，用jsp+struts2+hibernate，bootstrap等技术改写.
- 在线体验地址：[http://112.126.100.206:8080](http://112.126.100.206:8080)
- 在线电子相册：首页滑到电子相册区域单机。

#### 软件架构
软件架构说明
请使用myeclilse编辑此项目，或者eclipse。安装好tomcat，mysql等工具


#### 安装教程

1.  下载项目导入到eclipse中，数据库文件在PHP当中
2.  注意修改成你自己的数据库连接参数
![输入图片说明](https://images.gitee.com/uploads/images/2020/0301/150927_7017999b_5382722.png "屏幕截图.png")
3.  xxxx

#### 使用说明

1. 启动Tomcat，或者自带的myeclipse服务器
 ![输入图片说明](https://images.gitee.com/uploads/images/2020/0301/151034_da1c8851_5382722.png "屏幕截图.png")
2.  部署项目到服务器
![输入图片说明](https://images.gitee.com/uploads/images/2020/0301/151123_7b7b7fe7_5382722.png "屏幕截图.png")
3.  浏览器上输入地址注意修改成你自己的端口号
![输入图片说明](https://images.gitee.com/uploads/images/2020/0301/151253_33ed01fc_5382722.png "屏幕截图.png")



部分项目截图：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0301/154735_f5a32458_5382722.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0301/154822_9199de14_5382722.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0301/154958_14fb2aa2_5382722.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0301/160126_aa5c796c_5382722.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0301/160336_d2a6035b_5382722.png "屏幕截图.png")
![电子相册图](./gitImg/电子相册动画.gif) 
- 在线体验地址：[http://112.126.100.206:8080](http://112.126.100.206:8080)
- 在线电子相册：[电子相册](http://112.126.100.206:8080/jsp/camera.jsp)


项目视频：

<video src="./项目演示视频.mp4" controls="controls" width="800" height="600">您的浏览器不支持播放该视频！</video>
![项目视频](./项目演示视频.mp4) 
<embed src="./项目演示视频.mp4">

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

演示看项目中的视频文件


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

