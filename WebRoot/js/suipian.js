function suipian(){
		//使用这个页面必须有一个id=img的盒子
		var img = $("#img");
		//img.css("width",document.body.clientWidth);
			//获取屏幕宽高
		var divWidth = parseFloat(img.css("width"));
		var divHeight = parseFloat(img.css("height"));
		//console.log(divWidth+" "+divHeight);
		var rows = 5;
		var cols = 10;//设置碎片盒子10行5列
		var mwidth = divWidth/cols;//保存单个盒子的宽度和高度,每列10个
		var mheight = divHeight/rows;//保存每个盒子的高度，共5行
		//console.log(mwidth+" "+mheight);
		var divArray = new Array();
		var count = 0;//用于下标的统计，最后一个会保存的数为所有的元素数1
		for(var i=0;i<rows;i++)
		{
			var positionY ="-"+i* mheight+"px";//图片切割的y坐标，每一行的高度是一样的
			var mtop=i*mheight;//元素的位置
			for(var j=0;j<cols;j++)
			{
				var positionX ="-"+j * mwidth+"px";//图片切割的x坐标，每个元素的position x是不同的
				var mleft=j*mwidth;
				$tmp=$("<div></div>")
				$tmp.css({
				"position":"absolute",
				"top":mtop+"px",
				"left":mleft+"px",
				"float":"left",
				"display":"block",
				"width":mwidth+"px",
				"height":mheight+"px",
				//"background":"red",
				"background":"url('') "+positionX+" "+positionY,
				"overflow":"hidden",
				"border":"none"
				})
				//console.log("i:"+i+" j:"+j+" "+positionX+" "+positionY);
				img.append($tmp);//追加到盒子里面
				divArray[count]=$tmp;//保存到盒子数组中，方便动画使用
				divArray[count].left = mleft;//保存left
				divArray[count].top = mtop;//保存top;
				count++;
			}//endfor j
		}//endfor i
		//console.log(count);//调试信息
		
		//img.empty();清空盒子
		
		//下面这个for循环是核心块
		//碎片化进入,参数是一个jqury 图片切块集合
		function suipianIn(imgdiv)
		{
			var time;//随机动画时间
			imgdiv.hide();//隐藏所有块
			for(var i=0;i<cols*rows;i++)//这个循环是把切块放进来,回到divArray保存的left，top
			{
				time=Math.round(500+Math.random()*1000); // 可均衡获取 500 到 2000 的随机整数。,round 4舍5入
				//console.log("time is:"+time);//输出时间是否正确
				var tleft = divArray[i].left;
				var ttop = divArray[i].top;
				imgdiv.eq(i).show().animate({ 
					left:tleft,
					top:ttop
					  }, time,"swing" );
			}
		}//end suipianIn
		//碎片化进入结束
		
		//碎片化出去，切块放在左右下角，参数是一个jqury 图片切块集合
		function suipianOut(imgdiv)
		{
			var time;
			for(var i=0;i<cols*rows;i++)//这个循环是把切块放在左右下角
			{
				time=Math.round(500+Math.random()*1000); // 可均衡获取 1000 到 2000 的随机整数。,round 4舍5入
				var tleft = divArray[i].left;
				var ttop = divArray[i].top;
				if(i%4==0)//放左上角
				{
					imgdiv.eq(i).animate({
					left:-mwidth,
					top:-mheight
					  }, time,"swing" );
				}
				else if(i%4==1)//放右上角
				{
					imgdiv.eq(i).animate({ 
					left:divWidth+mwidth,
					top:-mheight
					  }, time,"swing" );
				}
				else if(i%4==2)//左下角
				{
					imgdiv.eq(i).animate({ 
						left:-mwidth,
						top:divHeight+mheight
					  }, time,"swing" );
				}
				else//3放右下角
				{
					imgdiv.eq(i).animate({
						left:divWidth+mwidth,
					top:divHeight+mheight},time,"swing");
				}
			}//end for
		}//end function suipian out
		var imgdiv=$("#img div");//获取所有图片切块的集合
		//添加一个透明的黑色区域放在背景图上
		var blackblock=("<div id='blackblock'></div>");
		img.append(blackblock);
		blackblock=$("#blackblock");
		blackblock.css({
			"width":"100%",
			"height":divHeight,
			"background":"rgba(21,21,21,0.7)"
		});
		//添加完毕
		var csrc=1;//图片共有四张1-4png
		setInterval(function(){
			suipianOut(imgdiv);
		},3000);
		setInterval(function(){
			suipianIn(imgdiv);
		},5000);
		var count=0;
		setInterval(function()
		{
		  
			 count++;
			 if(count>4)count=1;
			 var imgurl = "url('./img/sign/"+count+".png')";
			// console.log(imgurl);
		     imgdiv.css("backgroundImage",imgurl);
		  
		},5000);
}