 		$plist = $("#date p");
 		var weekday=["星期日","星期一","星期二","星期三","星期四","星期五","星期六"];
		function setDate()
		{
			var date = new Date();
			var h=date.getHours();
			var m=date.getMinutes();
			var s=date.getSeconds();
			if(h<10)
				h="0"+h;
			if(m<10)
				m="0"+m;
			if(s<10)
				s="0"+s;
			var month=date.getMonth()+1;
			if(month<10)
				month="0"+month;
			$plist.eq(3).text(h+":"+m+":"+s);
			$plist.eq(0).text(date.getDate());
			$plist.eq(1).text(weekday[date.getDay()]);
			$plist.eq(2).text(date.getFullYear()+"."+month);
		}
		setDate();
		setInterval(function(){
		setDate();
		},1000);
		//右边的日期获取结束