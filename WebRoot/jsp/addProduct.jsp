<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page language="java" import="DataBase.DataBase"%>
<%@ page language="java" import="java.sql.*"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<title>用户增加</title>
<link rel="stylesheet" href="css/bootstrap3.3.7.css">
<link rel="stylesheet" href="css/bootstrap-theme.min.css">
<link rel="stylesheet" href="css/wcCss.css">
<style>
	
</style>
</head>

<body>
	<div class="container">
		<div class="row">
			<div class="head">
				<button type="button" class="btn btn-success" data-toggle="modal"
					data-target="#myModal">
					<span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>添加商品
				</button>
				<!-- Modal -->
				<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
					aria-labelledby="myModalLabel">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"
									aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								<h4 class="modal-title" id="myModalLabel">添加商品</h4>
							</div>
							<div class="modal-body">
								<form action="productAction!adminAddProduct" >
									<div class="form-group">
										<label for="name">商品名称</label> <input type="text"
											class="form-control" id="name" name='name' placeholder="请输入商品名称">
									</div>
									<div class="form-group">
										<label for="scount">数量</label> <input type="text"
											class="form-control" id="scount" name="scount" placeholder="请输入数量">
									</div>
									<div class="form-group">
										<label for="price">价格</label> <input type="text"
											class="form-control" id="price" name="price" placeholder="请输入价格">
									</div>
									<div class="form-group">
										<label for="url">URL</label> <input type="url"
											class="form-control" id="url" name="url" placeholder="请输入url">
									</div>
									<button type="button" class="btn btn-default btn-right"
										data-dismiss="modal">关闭</button>
									<button type="submit" class="btn btn-primary" id="addUser">添加</button>
								</form>
							</div>

						</div>
					</div>
				</div>
			</div>
			<%
				// 获取数据库中的数据
				DataBase database = new DataBase();
				database.setTable("product");
				ResultSet re = database.query();
			%>
			<!-- 显示商品列表 -->
			<div class="table-responsive">
				<table class="table table-hover table-bordered">
					<caption class="tHead">商品信息</caption>
					<thead>
						<tr>
							<td>ID</td>
							<td>商品名称</td>
							<td>数量</td>
							<td>价格</td>
							<td>URL</td>
						</tr>
					</thead>
					<%
						while(re.next()){
							out.print("<tr>");
							out.print("<td>"+re.getInt("id")+"</td>");
							out.print("<td class='omit'>"+re.getString("name")+"</td>");
							out.print("<td>"+re.getInt("scount")+"</td>");
							out.print("<td>"+re.getDouble("price")+"</td>");
							out.print("<td>"+re.getString("url")+"</td>");
							out.print("</tr>");
						}
					%>
				</table>
			</div>
			<nav aria-label="Page navigation" style="text-align: center">
			<ul class="pagination">
				<li><a href="#" aria-label="Previous"> <span
						aria-hidden="true">&laquo;</span>
				</a></li>
				<li><a href="#">1</a></li>
				<li><a href="#">2</a></li>
				<li><a href="#">3</a></li>
				<li><a href="#">4</a></li>
				<li><a href="#">5</a></li>
				<li><a href="#" aria-label="Next"> <span aria-hidden="true">&raquo;</span>
				</a></li>
			</ul>
			</nav>
		</div>
	</div>
</body>
<script src="js/jquery-1.12.4.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
	$(function(){
		/*状态码判断*/
		<%
			String err = (String)session.getAttribute("err");
			if (err != null){
				if(err.equals("suc")){
					out.print("alert('恭喜你添加商品成功！')");
				}
				else if(err.equals("900")){
					out.print("alert('添加失败！该商品已存在')");
				}
				session.removeAttribute("err");//清除err状态吗
			}
		%>
	});
</script>
</html>
