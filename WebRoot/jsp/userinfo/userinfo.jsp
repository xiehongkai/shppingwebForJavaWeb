<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="Po.User"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'personinfo.jsp' starting page</title>
    <meta charset="UTF-8">
     <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	 <title>个人信息</title>
    <link rel="stylesheet" href="css/bootstrap3.3.7.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/wcCss.css">
    <link rel="stylesheet" type="text/css" href="./css/userspace.css">
    
    <style>
        .personal-info {
            max-width: 80%;

        }

        .head h3 {
            display: inline;
        }

        .head p {
            margin: 0;
        }

        .row-left {
            background-color: #eeeeee;
            margin-top: 15px;
        }

        .row .touxiang {
            max-width: 150px;
            margin: 50px auto;
        }

        .col-md-3 {
            border-right: 1px solid #5e5e5e;
        }
        .personal-form {
            margin: 20px;
        }
        .form-horizontal .control-label {
            padding-left: 0;
            padding-right: 20px;
        }
       #center {width: 63%; left: 226px;}
       #ul{width:960px;top:0px;line-height:50px;}
       #body{top:1px;}
    </style>
   <script type="text/javascript" src="./js/1.js"></script>
  </head>
<body>
  	<%User user = (User)session.getAttribute("user");
  		//如果获取为空就报状态码，说明超时
  		if (user == null)
  			{	
  				response.sendRedirect("userAction");
  				session.setAttribute("err", "1");
  			}
  	 %>
  	 <div id='top'></div>
	<div id='sort'>
		<ul id='ul'>
			<a href='./jsp/index.jsp'><li><p>网站首页</p></li></a>
			<a href='./jsp/show_product.jsp'><li><p>产品展示</p></li></a>
			<a href='./jsp/liuyan.jsp'><li><p>顾客留言</p></li></a>
			<a href='./jsp/we.jsp'><li><p>关于我们</p></li></a>
			<a href='./jsp/lianxi.jsp'><li><p>联系我们</p></li></a>
		</ul>
	</div>
	<div id="body">
		<!-- 左边区域 -->
		<div id="left">
				<ul>
					<span>全部功能</span>
					<a href='./jsp/userinfo/userinfo.jsp' target='iframe'><li class="select">我的资料</li></a>
					<a href='./jsp/userinfo/shoppingCar.jsp' target='iframe'><li>我的购物车</li></a>
					<a href='./jsp/userinfo/myOrder.jsp' target='iframe'><li>我的订单</li></a>
					<a href='./jsp/userinfo/getAddress.jsp' target='iframe'><li>收货地址</li></a>
				</ul>
		</div>
		<!-- 中间区域 -->
		<div id="center">
			<div class="container personal-info">
	    <div class="head">
	        <h3>我的信息</h3>
	        <p>在这里可以查看和修改您的信息</p>
	    </div>
	    <div class="row row-left">
	        <div class="col-md-3">
	            <img src="./img/tx.png" alt="图片暂无显示" class="img-responsive img-circle touxiang">
	        </div>
	        <div class="col-md-9">
	            <form action='userAction!userUpdateInfo' class="form-horizontal personal-form">
	                <div class="form-group">
	                    <label for="username" class="col-sm-2 control-label">用户名</label>
	                    <div class="col-sm-10">
	                        <input type="text" class="form-control" id="username" name="username" readonly='readonly' value="<%=user.getUsername()%>">
	                    </div>
	                </div>
	                <div class="form-group">
	                    <label for="password" class="col-sm-2 control-label">密码</label>
	                    <div class="col-sm-10">
	                        <input type="password" class="form-control" id="password" name="password" placeholder="密码" value="<%=user.getPassword()%>">
	                    </div>
	                </div>
	                <div class="form-group">
	                    <label for="phone" class="col-sm-2 control-label">联系电话</label>
	                    <div class="col-sm-10">
	                        <input type="text" class="form-control" id="phone" placeholder="联系电话" name="phone" value="<%=user.getPhone()%>">
	                    </div>
	                </div>
	                <div class="form-group">
	                    <label for="email" class="col-sm-2 control-label">电子邮箱</label>
	                    <div class="col-sm-10">
	                        <input type="text" class="form-control" id="email" name="email" placeholder="电子邮箱" value="<%=user.getEmail()%>">
	                    </div>
	                </div>
	                <div class="form-group">
	                    <label for="name" class="col-sm-2 control-label">姓名</label>
	                    <div class="col-sm-10">
	                        <input type="search" class="form-control" id="name" name="name" placeholder="姓名" value="<%=user.getName()%>">
	                    </div>
	                </div>
	                <div class="form-group">
	                    <div class="col-sm-offset-2 col-sm-10">
	                        <button type="submit" name="uinfo_sub" class="btn btn-primary">保存</button>
	                    </div>
	                </div>
	            </form>
	        </div>
	    </div>
	</div>	
		</div>
		<!-- 右边区域 -->
		<div id="right">
			<div id="calendar">
				我的日历
				<span></span>
			</div>
			<div id="date">
				<p></p>
				<p></p>
				<p></p>
				<p></p>
			</div>
		</div>
		<div id="bottom">
		</div>
	</div>
	<script src="./js/userspace/user.js"></script>
</body>
</html>
