<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page language="java" import="Po.CarProduct"%>
<%@ page language="java" import="DataBase.DataBase"%>
<%@ page language="java" import="java.sql.ResultSet"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'shoppingCar.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>购物车</title>
    <link rel="stylesheet" href="css/bootstrap3.3.7.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/wcCss.css">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<link rel="stylesheet" type="text/css" href="./css/userspace.css">
	<style>
        .car-top {
            height: 100px;
            /* line-height: 100px; */
            color: #b0b0b0;
            border-bottom: 2px solid #ff6700;
        }

        .inner-warp {
            width: 1200px;
            margin: 0px auto;
            /* overflow: hidden; */
        }

        .car-top .car-title {
            font-size: 28px;
            line-height: 48px;
            font-weight: normal;
            color: #424242;
            float: left;
            margin-left: 45px;
            margin-top: 26px;
        }

        .car-top .title-info {
            display: inline-block;
            margin-top: 48px;
            margin-left: 20px;
        }
        .container {
            text-align: center;
            max-width: 100%;
        }
        .img-car {
            max-width: 50px;
            display: inline-block;
        }
        
        .table {
            border: 1px solid black;
        }
        td {
            padding-top: 25px !important;
            font-size: 18px;
        }
        .no-padding {
           padding-top: 0 !important;
            font-size: 16px;
        }
        .table-foot {
            float: left;
            width: 100%;
            margin-top: 15px;
            background-color: #eeeeee;
            padding: 10px;
        }
        .foot-left {
            float: left;
            font-size: 16px;
            margin-top: 5px;
        }
        .foot-left .red {
            color: red;
        }
        .foot-right {
            float: right;
        }
        .btn-success {
            font-size: 18px;
        }
        #center {
        	width: 80%;
        	background-color: #fff;
        }
       #ul{width:960px;top:0px;line-height:50px;}
       #body{top:1px;}
    </style>
  </head>
  <script type="text/javascript" src='./js/1.js'></script>
  <body>
  	<%
  		String userid = (String)session.getAttribute("userid");
  		if(userid==null)
		{
			session.setAttribute("err","1");
			response.sendRedirect("userAction");//转到首页
		}
		String count=null;//统计
		DataBase datebase = new DataBase();
		datebase.setTable("carproduct");
		ResultSet rs=datebase.query("select * from "+datebase.getTable()+" where uid='"+userid+"'");
		if(rs==null)
			out.print("<script>alert('您的购物车为空')</script>");
  	 %>
  	<div id='top'></div>
	<div id='sort'>
		<ul id='ul'>
			<a href='./jsp/index.jsp'><li><p>网站首页</p></li></a>
			<a href='./jsp/show_product.jsp'><li><p>产品展示</p></li></a>
			<a href='./jsp/liuyan.jsp'><li><p>顾客留言</p></li></a>
			<a href='./jsp/we.jsp'><li><p>关于我们</p></li></a>
			<a href='./jsp/lianxi.jsp'><li><p>联系我们</p></li></a>
		</ul>
	</div>
	<div id="body">
		<!-- 左边区域 -->
		<div id="left">
				<ul>
					<span>全部功能</span>
					<a href='./jsp/userinfo/userinfo.jsp' target='iframe'><li>我的资料</li></a>
					<a href='./jsp/userinfo/shoppingCar.jsp' target='iframe'><li class='select'>我的购物车</li></a>
					<a href='./jsp/userinfo/myOrder.jsp' target='iframe'><li>我的订单</li></a>
					<a href='./jsp/userinfo/getAddress.jsp' target='iframe'><li>收货地址</li></a>
				</ul>
		</div>
		<!-- 中间区域 -->
		<div id="center">
			<div class="container">
    <div class="car-top">
        <div class="inner-warp">
            <h3 class="car-title">
                <span class="glyphicon glyphicon-shopping-cart"></span>我的购物车
            </h3>
            <span class="title-info">
                温馨提示：产品是否购买成功，以最终下单为准哦，请尽快结算
            </span>
        </div>
    </div>
    <div class="container">
        <div class="shop-car">
            <form action="#">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <caption class="tHead">购物车列表</caption>
                        <thead>
                        <tr>
                            <td>是否选中</td>
                            <td>商品名称</td>
                            <td>单价</td>
                            <td>数量</td>
                            <td>小计</td>
                            <td>操作</td>
                        </tr>
                        </thead>
                        <%
                        	if(rs==null)
                        	 out.println("<tr><td cospan='6'>购物车为空</td></tr>");
                         	else
                         		while(rs.next())
                         	{
                         		String pid = ""+rs.getInt(8);
                         		String pname =rs.getString(2);
                         		String imgurl = "./img/pro/pro2/"+pid+"/1.jpg";
                         		out.println("<tr>");
			                         		out.println("<td>");
			                         			out.println("<input type='checkbox'>");
			                         		out.println("</td>");
			                         		out.println(" <td class='no-padding'>");
			                         		out.println("<img src="+imgurl+" title='"+pname+"' class='img-responsive img-rounded img-car'>");
			                         		out.println("</td>");
			                         		out.println(" <td>69元</td>");
			                         		out.println(" <td>1</td>");
			                         		out.println(" <td>69元</td>");
			                         		out.println(" <td>");
			                         			out.println("<button type='button' class='btn btn-warning btn-sm'>");
			                         				out.println("<span class='glyphicon glyphicon-trash' aria-hidden='true'></span>删除");
			                         			out.println("</button>");
			                         		out.println("</td>");
                          		 out.println("</</tr>");
                         	}
                         %>
                                
                                    
                                
                        
                    </table>
                    <div class="table-foot">
                        <div class="foot-left">
                        	<%
                        		rs=datebase.query("select count(*) from carproduct where uid='"+userid+"'"); 
                        		if(rs==null)
                        			out.println("<span>共 <em class='red'>0</em> 件商品</span>");
                        		else
                        		{
                        			rs.next();
                        			out.println("<span>共 <em class='red'>"+rs.getInt(1)+"</em> 件商品</span>");
                        		}
                        	%>
                            
                        </div>
                        <div class="foot-right">
                            <button type="submit" class="btn btn-success">去结账</button>
                        </div>

                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
		</div>
		<div id="bottom">
		</div>
	</div>
	<script src="./js/userspace/user.js"></script>
</body>
<script src="js/jquery-1.12.4.min.js"></script>
<script>
    $(function () {
        $("button[type=button]").click(function () {
            var tr = $(this).parent().parent();
            tr.remove();
        })
    });
</script>
  </body>
</html>
