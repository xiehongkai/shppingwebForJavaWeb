<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page language="java" import="DataBase.DataBase" %>
<%@ page language="java" import="java.sql.ResultSet" %>
<%@ page language="java" import="Po.User" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html lang="en">
<head>
	<!--设置本文档的路劲为  -->
	<base href="<%=basePath%>">
    <meta charset="UTF-8">
    <title>我的订单</title>
   <link rel="stylesheet" type="text/css" href="./css/userspace.css">
    <style>
        table{
            width:1000px;
            text-align: center;
            font-size: 18px;
            color:black;
       		 }
        table tr:nth-child(odd){
            background-color: #eae2c7;
        }
        table tr:nth-child(1){
            background-color:black;
            color:snow;
        }
        table tr{
            width:100px;
            height: 60px;
        }
        tr:hover{
            background-color: #eeeeee;
            color:#2aabd2;
        }
        input[type="button"]{
            font-size: 17px;
            font-family: "Microsoft YaHei UI";
            background-color: #d9ebe8;
        }
        #center {
        	width: 80%;
        	background-color: rgba(255,255,255,0.5);
        }
       #body{top:1px;left:-10px;}
       #left{width:17%;}
       #left ul{top:-17px};
    </style>
</head>

<body>
	<%	
		User user = (User)session.getAttribute("user");
		DataBase dataBase=null;
		ResultSet rs=null;
  		//如果获取为空就报状态码，说明超时
  		if (user == null)
  			{	
  				response.sendRedirect("userAction");
  				session.setAttribute("err", "1");
  			}
  		else//否则就获取当前登录用户的订单信息
  		{
  			dataBase = new DataBase();
  			rs=dataBase.query("select * from indent where uid ='"+user.getId()+"'");
  			if(rs==null)
  				out.print("<script>alert('没有查询到数据')</script>");
  		}
  	 %>
 <div id='top'></div>
	<div id='sort'>
		<ul id='ul'>
			<a href='./jsp/index.jsp'><li><p>网站首页</p></li></a>
			<a href='./jsp/show_product.jsp'><li><p>产品展示</p></li></a>
			<a href='./jsp/liuyan.jsp'><li><p>顾客留言</p></li></a>
			<a href='./jsp/we.jsp'><li><p>关于我们</p></li></a>
			<a href='./jsp/lianxi.jsp'><li><p>联系我们</p></li></a>
		</ul>
	</div>
	<div id="body">
		<!-- 左边区域 -->
		<div id="left">
				<ul>
					<span >全部功能</span>
					<a href='./jsp/userinfo/userinfo.jsp' target='iframe'><li>我的资料</li></a>
					<a href='./jsp/userinfo/shoppingCar.jsp' target='iframe'><li>我的购物车</li></a>
					<a href='./jsp/userinfo/myOrder.jsp' target='iframe'><li class='select'>我的订单</li></a>
					<a href='./jsp/userinfo/getAddress.jsp' target='iframe'><li>收货地址</li></a>
				</ul>
		</div>
		<!-- 中间区域 -->
		<div id="center">
			<table  align="center">
			    <tr>
			        <td>全选<input type="checkbox" value="1"></td>
			         <td>商品图片</td>
			        <td>宝贝名称</td>
			        <td>单价</td>
			        <td>订单创建时间</td>
			        <td>订单号</td>
			        <td>实付款</td>
			        <td>订单发货状态</td>
			        <td>订单完成状态</td>
			        <td>订单操作</td>
			    </tr>
			    <%
			     	if(rs==null)
			     		out.println("<tr><td colspan='9'>没有查询到数据</td></tr>");
			     	else
		     			while(rs.next())
		     			{
		     				out.println("<tr>");
		     				out.println("<td><input type='checkbox' value= </td>");
			        		out.println("<td><img src='./img/pro/pro2/"+rs.getInt(18)+"/1.jpg' width='64px' height='64px' alt='图片无法加载'></td>");
			        		out.println("<td>"+rs.getString(3)+"</td>");
			        		out.println("<td>"+rs.getDouble(7)+"</td>");
			        		out.println("<td>"+rs.getDate(15)+"</td>");
			        		out.println("<td>"+rs.getString(1)+"</td>");
			        		out.println("<td>"+rs.getDouble(8)+"</td>");
			        		out.println("<td>"+rs.getString(12)+"</td>");
			        		out.println("<td>"+rs.getString(14)+"</td>");
			        		out.println("<td><input type='button' value='删除'></td>");
			        		out.println("</tr>");
		     			}
			     %>
			    <tr>
			        <td colspan="2"><input type="button" value="删除"></td>
			    </tr>
		</table>
		</div>
		<div id="bottom">
		</div>
	</div>
  </body>
</body>
</html>

