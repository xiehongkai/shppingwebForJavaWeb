<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()	+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<html>
<head>
<base href="<%=basePath%>">
<title>我的简易购物网站</title>
<meta charset='utf-8'>
   <link rel="stylesheet" type="text/css" href="./css/userspace.css">
	<style>
	body{
	margin:0px;
	padding:0px;
	}
 
 #bodyInside{
	  width:400px;
	  height:600px;
	 margin:10px 185px;
	  }
  #body label {
	  cursor:pointer;
	  display:inline-block;
	  font-size:20px;
	  width:130px;
	  text-align:right;
	  font-weight:bold;
	  vertical-align:top;
	  
  }
  input{
	  font-size:inherit;
	  width:200px;
	  height:30px;
  }
 input[type=submit]{
     width:100px;
	 height:40px;
	 background-color:skyblue;
	 color:purple;
	 font-weight:bold;
	 font-size:18px;
	 text-align:center;
	 margin:10px 0px 0px 180px;;
 }
 span{
  color:red;
 }
  #center
{
  	width: 80%;
   	background-color: rgba(255,255,255,0.5);
}
#body{top:1px;}
#left{width:17%;}
#left ul{top:-17px};
</style>
</head>

<body>
<div id='top'></div>
	<div id='sort'>
		<ul id='ul'>
			<a href='./jsp/index.jsp'><li><p>网站首页</p></li></a>
			<a href='./jsp/show_product.jsp'><li><p>产品展示</p></li></a>
			<a href='./jsp/liuyan.jsp'><li><p>顾客留言</p></li></a>
			<a href='./jsp/we.jsp'><li><p>关于我们</p></li></a>
			<a href='./jsp/lianxi.jsp'><li><p>联系我们</p></li></a>
		</ul>
	</div>
	<div id="body">
		<!-- 左边区域 -->
		<div id="left">
				<ul>
					<span>全部功能</span>
					<a href='./jsp/userinfo/userinfo.jsp' target='iframe'><li>我的资料</li></a>
					<a href='./jsp/userinfo/shoppingCar.jsp' target='iframe'><li>我的购物车</li></a>
					<a href='./jsp/userinfo/myOrder.jsp' target='iframe'><li>我的订单</li></a>
					<a href='./jsp/userinfo/getAddress.jsp' target='iframe'><li class='select'>收货地址</li></a>
				</ul>
		</div>
		<!-- 中间区域 -->
		<div id="center">
			<div id="bodyInside">
  <form action="myshoppingweb/we.php" method="post">
	<label><span>*</span>收货人姓名：</label><input type="text" maxlength="20" required="required"><br><br>
	<label>省份：</label>
	  <select>
	         <option value="guangdong">广东省</option>
	  </select>
	  <select> 
	  		<option value="guangzhou">广州市</option>
	  </select>
	  <select> 
	  	 <option value="1">荔湾区</option>
		 <option value="2">越秀区</option>
		 <option value="3">海珠区</option>
		 <option value="4">天河区</option>
		 <option value="5">白云区</option>
		 <option value="6">黄埔区</option>
		 <option value="7">番禺区</option>
		 <option value="8">花都区</option>
		 <option value="9">南沙区</option>
		 <option value="10">从化区</option>
		 <option value="11">增城区</option>
	  </select>
		<br><br>
		<label><span>*</span>详细地址：</label><input type="text" maxlength="20" required="required"><br><br>
		<label><span>*</span>手机号码：</label><input type="text" maxlength="20" required="required"><br><br>
		<label><span>*</span>固定电话：</label><input type="text" maxlength="20"required="required"><br><br>
		<label><span>*</span>电子邮件：</label><input type="text" maxlength="20" required="required"><br><br>
		<label>备注：</label>
       <textarea cols="25" rows="6" align="left">
	   </textarea>
	<input type="submit" value="添加地址"> <br><br>
</form>
</div>
</div>
		</div>
	</div>
		<div id="bottom">
		</div>
</div>
</body>
</html>
