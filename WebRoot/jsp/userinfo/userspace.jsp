<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()	+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>个人空间</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" type="text/css" href="./css/userspace.css">
	<script src='./js/1.js'></script>
  </head>
  
  <body>
    <div id='top'></div>
	<div id='sort'>
		<ul id='ul'>
			<a href='./jsp/index.jsp'><li><p>网站首页</p></li></a>
			<a href='./jsp/show_product.jsp'><li><p>产品展示</p></li></a>
			<a href='./jsp/liuyan.jsp'><li><p>顾客留言</p></li></a>
			<a href='./jsp/we.jsp'><li><p>关于我们</p></li></a>
			<a href='./jsp/lianxi.jsp'><li><p>联系我们</p></li></a>
		</ul>
	</div>
	<div id="body">
		<!-- 左边区域 -->
		<div id="left">
				<ul>
					<span style="background: rgb(255,68,1)">全部功能</span>
					<a href='./jsp/userinfo/userinfo.jsp' target='iframe'><li>我的资料</li></a>
					<a href='./jsp/userinfo/shoppingCar.jsp' target='iframe'><li>我的购物车</li></a>
					<a href='./jsp/userinfo/myOrder.jsp' target='iframe'><li>我的订单</li></a>
					<a href='./jsp/userinfo/getAddress.jsp' target='iframe'><li>收货地址</li></a>
				</ul>
		</div>
		<!-- 中间区域 -->
		<div id="center">
		</div>
		<!-- 右边区域 -->
		<div id="right">
			<div id="calendar">
				我的日历
				<span></span>
			</div>
			<div id="date">
				<p></p>
				<p></p>
				<p></p>
				<p></p>
			</div>
		</div>
		<div id="bottom">
		</div>
	</div>
	<script src="./js/userspace/user.js"></script>
  </body>
</html>
