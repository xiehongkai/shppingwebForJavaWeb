<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page language="java" import="Po.Indent"%>
<%@ page language="java" import="DataBase.DataBase"%>
<%@ page language="java" import="java.sql.ResultSet"%>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'buysuccess.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<style>
	*{
		margin:0px;
		padding:0px;
	}
	body
	{
		height:1100px;
		width:100%;
		background:#f0f0f0;
	}
	#top
	{
		width:100%;
		height:120px;
		background:transparent url(img//log.jpg) no-repeat scroll center center;
		overflow:hidden;
		z-index:1;
	}
	#sort
	{
		position:relative;
		width:100%;
		height:50px;
		line-height:50px;
		background:#f0f0f0;
		margin-bottom:8;
		left:50%;
		margin-left:-30%;
	}
	#ul li
	{	
		color:white;
		margin:0 1px;
		float :left;
		list-style-type:none;
		height:100%;
		background:rgba(194,0,0,0.3);
		width:180px;
		text-align:center;
	}
	#ul li a
	{
		color:white;
		margin:0 1px;
		text-decoration:none;
	}
	#body
	{	
		overflow:visible;
		width:100%;
		height:830px;
		z-index:1;
		position:relative;
		background:url("./img/bg2.png");
	}
	#body_left
	{
		width:20%;
		height:100%;
		position:absolute;
		top:0px;
		left:20px;
		z-index=1;
	}
	#body_left ul
	{
		position:absolute;
		padding:0px;
		top:75px;
		left:0px;
		margin:0px;
	}
	#body_left li
	{
		width:250px;
		height:75px;
		background:rgb(227,169,169);
		z-index=3;
		list-style:none;
		display:block;
		margin:0px;
		text-align:center;
		border: 0.2px solid #f0f0f0;
	}
	#body_left li p
	{
		margin:25px auto;
	}
	#body_left_img1 img
	{
		width:250px;
		height:75px;
		position:absolute;
		top:0px;
		left:0px;
		margin:0px;
	}
	#body_left_img2 img
	{
		width:250px;
		height:530px;
		z-index:3;
		position:absolute;
		top:300px;
		left:0px;
	}
	#body #center
	{
		position:relative;
		width:80%;
		height:822px;
		top:0px;
		left:20%;
		z-index:3;
		background:rgba(0,0,0,0.5);
	}
	 #successTop 
	{
		width:100%;
		height:500px;
		position:absolute;
		top:0px;
	}
	#TopLeft 
	{
		width:50%;
		height:400px;
		position:absolute;
		top:0px;
		left:0px;
	}
	#TopLeft img
	{
		width: 100%;
	    height: 400;
	    position: absolute;
	    float: left;
	    top: 0px;
	    left: 0px;
	}
	#TopRight
	{
	    width: 50%;
	    height: 400px;
	    z-index: 0;
	    text-align: center;
	    position: absolute;
	  	right:0px;
	    top: 0px;
	    background:rgba(255,255,255,0.5);
	}
	#TopRight ol
	{
		width:100%;
		height:100%;
		list-style:none;
	}
	#TopRight ol li
	{
		width:100%;
		height:80px;
		line-height:80px;
		color:black;
	}
	#TopRight ol li span
	{
		position:absolute;
		width:30%;
		left:0px;
		text-align:center;
		color:black;
		font-weight:bold;
		overflow: hidden;
		text-overflow: ellipsis;
		white-space: nowrap; /*文本不换行，这样超出一行的部分被截取，显示...*/
	}
	#TopRight ol li .span2
	{
		position:absolute;
		width:70%;
		left:30%;
		text-align:center;
		color:rgb(255,22,84);
		font-weight:bold;
	}
	 #successBottom
	{
		width:100%;
		height:50px;
		line-height:50px;
		position:absolute;
		top:400px;
		color:rgb(255,22,84);
		font-size:25px;
		text-align:center;
		border:1px solid red;
		background:rgba(255,255,255,0.7);
	}
	#bottom
	{
		width:100%;
		height:125px;
		position:absolute;
		z-index:5;
		top:1000px;
		background:url("img/bottom.jpg");
	}
	#guess
	{
		width:100%;
		position:absolute;
		bottom:0px;
		height:370px;
	}
	#guess .product
	{
		width:242px;
		float:left;
		height:350px;
		margin:9px;
		border:1px solid black;
		overflow:hidden;
	}
	/* 定义了一个边框动画 */
	@keyframes mybordermove
	{
		0%{border:1px solid black;}
		100%{border:2px solid orange;}
	}
	#guess .product:hover
	{
		animation:mybordermove 0.5s;
		border:2px solid orange;
	}
	#guess .product img
	{
		display:block;
		width:100%;
		height:350px;
	}
	#guess .product .blackname
	{
		width:242px;
		height:100px;
		border:1px solid red;
		position:absolute;
		bottom:10px;
		background:rgba(0,0,0,0.5);
		display:none;
	}
</style>
<script src="./js/1.js"></script>
<body>
	<%
		//获取订单中的session对象
		Indent indent = (Indent)session.getAttribute("indent");
		if(indent==null)
		 response.sendRedirect("./jsp/404.jsp");
		String imgsrc="./img/pro/pro2/"+indent.getProduct().getId()+"/1.jpg";
		//获取猜你喜欢的商品
		DataBase datebase = new DataBase();
		datebase.setTable("product");
		//获取最大的id
		ResultSet rs=datebase.query("select max(id) from "+datebase.getTable());
		int maxid =0;
		if(rs ==null)
			maxid = 0;
		else rs.next();
		maxid = rs.getInt(1);
		int [] id = new int[maxid+1];//保存共maxid+1,产品id0没有，所以不用个数组
		int [] tid = new int[4];//保存生成的4个id
		String psrc[] = new String[4];//保存生成的4个产品url
		for(int i=1;i<=maxid;i++)//初始化为-1代表没有被取到,1表示被占用了
			id[i]=-1;
		
		//随机生成4个不重复出现的产品id	
		for(int i=0;i<4;)
		{
			int temp =(int) (Math.random()*18+1);
			if(id[temp]==1)continue;//出现过就重新生成
			if(temp==indent.getProduct().getId())continue;//如果和当前购买的id相同就重新生成
			else
			{
				id[temp]=1;
				tid[i]=temp;
				psrc[i]="./img/pro/pro2/"+temp+"/";
				//out.println(psrc[i]);
				i++;
			}
			//out.println(temp);
		}
	 %>
	<div id='top'></div>
	<div id='sort'>
		<ul id='ul'>
			<a href=''><li><p>网站首页</p></li></a>
			<a href='./jsp/show_product.jsp'><li><p>产品展示</p></li></a>
			<a href='./jsp/liuyan.jsp'><li><p>顾客留言</p></li></a>
			<a href='./jsp/we.jsp'><li><p>关于我们</p></li></a>
			<a href='./jsp/lianxi.jsp'><li><p>联系我们</p></li></a>
		</ul>
	</div>
	<div id='body'>
		<div id='body_left'>
			<div id='body_left_img1'>
				<img src='img/left.jpg'>
			</div>
			<div id='ul'>
				<ul>
				<li><p><a href='./jsp/we.jsp'>公司简介</a></p></li>
				<li><p><a href='./jsp/liuyan.jsp'>顾客留言</a></p></li>
				<li><p><a href='./jsp/lianxi.jsp'>联系我们</a></p></li>
				</ul>
			</div>
			<div id='body_left_img2'>
				<img src='img/left2.jpg'>
			</div>
		</div>
		<div id='center'>
				   <div id="successTop">
						<div id="TopLeft"><img src="<%=imgsrc %>" alt="图片无法加载"></div>
						<div id="TopRight">
							<ol>
								<li><span>商品名称:</span><span class='span2'><%=indent.getPname() %></span></li>
								<li><span>尺寸:</span><span class='span2'><%=indent.getSize() %></span></li>
								<li><span>颜色:</span><span class='span2'><%=indent.getColor() %></span></li>
								<li><span>数量:</span><span class='span2'><%=indent.getNumber() %></span></li>
								<li><span>共付款:</span><span class='span2'><%=indent.getTprice() %></span></li>
							</ol>
						</div>
				   </div>
				   <div id="successBottom">
					   购买成功(下面商品您可能感兴趣)
				   </div>
				   <div id="guess">
				   		<a href="productAction!proInfo?id=<%=tid[0]%>"><div class='product'><div class='blackname'></div><img src="<%=psrc[0]+"1.jpg"%>"></div></a>
				   		<a href="productAction!proInfo?id=<%=tid[1]%>"><div class='product'><div class='blackname'></div><img src="<%=psrc[1]+"1.jpg"%>"></div></a>
				   		<a href="productAction!proInfo?id=<%=tid[2]%>"><div class='product'><div class='blackname'></div><img src="<%=psrc[2]+"1.jpg"%>"></div></a>
				   		<a href="productAction!proInfo?id=<%=tid[3]%>"><div class='product'><div class='blackname'></div><img src="<%=psrc[3]+"1.jpg"%>"></div></a>
				   </div>
		</div>
	</div>
		 <div id='bottom'>
		
	</div> 
	
<script type="text/javascript">
	//定时器结束
	
	//鼠标移动判定开始
	$current = $("#ul li:eq(3)"); //匹配第4个列表元素
	$current2= $("#body_left li:first");
	$current.css("background","rgba(194,0,0,1)");
	$current2.css("background","rgba(194,0,0,1)");
	$("#ul li").click(function(){
		$("#ul li").css("background","rgba(194,0,0,0.3)");//淡色所有元素
		$(this).css("background","rgba(194,0,0,1)");//加深当前元素
		$current = $(this);//记录当前元素
	});
	
		$("#body_left li").click(function(){
		$("#body_left li").css("background","rgba(194,0,0,0.3)");//淡色所有元素
		$(this).css("background","rgba(194,0,0,1)");//加深当前元素
		$current2 = $(this);//记录当前元素
		$("#ul li:eq(3)").css("background","rgba(194,0,0,1)");
	});
	
	$("#ul li").hover(function(){
	$(this).css("background","rgba(194,0,0,1)");
	},function(){
	$(this).css("background","rgba(194,0,0,0.3)");
	$current.css("background","rgba(194,0,0,1)");//当前元素始终加深
	});
	
	$("#body_left li").hover(function(){
	$(this).css("background","rgba(194,0,0,1)");
	},function(){
	$(this).css("background","rgba(194,0,0,0.3)");
	$current2.css("background","rgba(194,0,0,1)");//当前元素始终加深
	});
	//鼠标移动判定结束
	$("#body").hide();
	$("#body").slideDown(1000);
</script>
</body>
</html>
