<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<html>
<head>
<title>我的简易购物网站</title>
<meta charset='utf-8'>
</head>
<style>
	*{
		margin:0px;
		padding:0px;
	}
	body
	{
		height:1100px;
		width:100%;
		background:#f0f0f0;
	}
	#top
	{
		width:100%;
		height:120px;
		background:transparent url(../img//log.jpg) no-repeat scroll center center;
		overflow:hidden;
		z-index:1;
	}
	#sort
	{
		position:relative;
		width:100%;
		height:50px;
		background:#f0f0f0;
		padding:0 12%;
		margin-bottom:8;
	}
	#ul li
	{	
		color:white;
		margin:0 1px;
		float :left;
		list-style-type:none;
		height:50px;
		line-height:50px;
		background:rgba(194,0,0,0.3);
		width:180px;
		text-align:center;
	}
	#ul li a
	{
		color:white;
		margin:0 1px;
		text-decoration:none;
	}
	#body
	{	
		overflow:visible;
		width:100%;
		height:830px;
		z-index:1;
		position:relative;
		background:white;
	}
	#body img
	{
		width:800px;
		height:800px;
		position:absolute;
		top:0px;
		left:21%;
		z-index:2;
	}
	#body #center p
	{
		position:absolute;
		top:0px;
		left:22%;
		z-index:3;
	}
	#body_left
	{
		width:250px;
		height:100%;
		position:absolute;
		top:0px;
		left:20px;
		z-index=1;
	}
	#body_left ul
	{
		position:absolute;
		padding:0px;
		top:75px;
		left:0px;
		margin:0px;
	}
	#body_left li
	{
		width:250px;
		height:75px;
		line-height:75px;
		background:rgb(227,169,169);
		z-index=3;
		list-style:none;
		display:block;
		margin:0px;
		text-align:center;
		border:0.2px solid #f0f0f0;
	}
	#body_left_img1 img
	{
		width:250px;
		height:75px;
		position:absolute;
		top:0px;
		left:0px;
		margin:0px;
	}
	#body_left_img2 img
	{
		width:250px;
		height:530px;
		z-index:3;
		position:absolute;
		top:300px;
		left:0px;
	}
	#companyDesc{
		display:block；
		margin:10px auto;
		width:800px;
		height:400px;
		color:black;
		position:absolute;
		line-height:22px;
		letter-spacing:2px;
		top:50px;
		left:22%;
		z-index:3;
	}
	#companyDesc h2{	
		text-align:center
	}
	
	#companyDesc label{
		font-size:18px;
		font-weight:bold;
		color:red;
	}
	#companyDesc .myvedio
	{
		width:800px;
		height:625px;
		position:absolute;
		bottom:0px;
		overflow:hidden;
		margin-top:10px;
	}
	#bottom
	{
		width:100%;
		height:125px;
		position:absolute;
		bottom:-125px;
		background:url("../img/bottom.jpg");
	}

	
	#body_right
	{
		background:url("../img/weright.jpg") -70px 0px;
		width:240px;
		height:900px;
		position:absolute;
		top:0px;
		right:0px;
	}
</style>
<script src="../js/1.js"></script>
<body>
	<div id='top'></div>
	<div id='sort'>
		<ul id='ul'>
			<a href='index.jsp'><li><p>网站首页</p></li></a>
			<a href='show_product.jsp'><li><p>产品展示</p></li></a>
			<a href='liuyan.jsp'><li><p>顾客留言</p></li></a>
			<a href='we.jsp'><li><p>关于我们</p></li></a>
			<a href='lianxi.jsp'><li><p>联系我们</p></li></a>
		</ul>
	</div>
	<div id='body'>
		<div id='body_left'>
			<div id='body_left_img1'>
				<img src='../img/left.jpg'>
			</div>
			<div id='ul'>
				<ul>
				<li><a href='we.jsp'>公司简介</a></li>
				<li><a href='liuyan.jsp'>顾客留言</a></li>
				<li><a href='lianxi.jsp'>联系我们</a></li>
				</ul>
			</div>
			<div id='body_left_img2'>
				<img src='../img/left2.jpg'>
			</div>
		</div>
		<div id='center'>
		<div>
			<p>
				你当前的位置>><a href='index.jsp'>网站首页</a>>>
				<a href=''>关于我们</a>
			</p>
			<!--<img src='img/we.jpg'>-->
			<div id="companyDesc">
				   <h2>公司简介</h2><br/><br/>
				   <label>&nbsp;&nbsp;&nbsp;&nbsp; 品牌简介：</label>
				  广州衣锦年华服饰有限公司旗下品牌“衣锦”潮流服饰创立于2010年，
				   是一家集生产与销售为一体的大型批发服饰。
				   本公司位于广州市白云区南村镇美悦工业园3栋。
				   “衣锦”一直推行韩版时尚潮流为主，设计风格来源于韩国时尚潮流。
				   成立至今，积累了丰富的市场经验。<br/><br/>
				 <label>&nbsp;&nbsp;&nbsp;&nbsp; 品牌定位</label>
				   22-35岁女性，她们行事果断，独立大方，既懂得追求时尚，又不断期待改变；
				   她们注重外在的气质修养，也追求内涵品质的提升；
				   她们有独到的品味，又懂得活出自我个性；
				   她们不会刻意追求奢华，却活得精致出色。
				   <div class="myvideo" style="margin-top:20px"> 
						<!--  
   						<video width="800px" height="520px" autoplay='autoplay' controls='controls' preload='preload' src='../mp4/衣锦年华女装店宣传视频.mp4'></video>
				   		-->
   						<video width="800px" height="450px" autoplay='true' controls='controls' preload='preload' src='../mp4/advertise.mp4'></video>
				   		
					</div>
			
			</div>
		</div>
		<div id='body_right'>
				
		</div>
	</div>
	 <div id='bottom'>
		
	</div> 
	
<script type="text/javascript">
	//定时器结束
	
	//鼠标移动判定开始
	$current = $("#ul li:eq(3)"); //匹配第4个列表元素
	$current2= $("#body_left li:first");
	$current.css("background","rgba(194,0,0,1)");
	$current2.css("background","rgba(194,0,0,1)");
	$("#ul li").click(function(){
		$("#ul li").css("background","rgba(194,0,0,0.3)");//淡色所有元素
		$(this).css("background","rgba(194,0,0,1)");//加深当前元素
		$current = $(this);//记录当前元素
	});
	
		$("#body_left li").click(function(){
		$("#body_left li").css("background","rgba(194,0,0,0.3)");//淡色所有元素
		$(this).css("background","rgba(194,0,0,1)");//加深当前元素
		$current2 = $(this);//记录当前元素
		$("#ul li:eq(3)").css("background","rgba(194,0,0,1)");
	});
	
	$("#ul li").hover(function(){
	$(this).css("background","rgba(194,0,0,1)");
	},function(){
	$(this).css("background","rgba(194,0,0,0.3)");
	$current.css("background","rgba(194,0,0,1)");//当前元素始终加深
	});
	
	$("#body_left li").hover(function(){
	$(this).css("background","rgba(194,0,0,1)");
	},function(){
	$(this).css("background","rgba(194,0,0,0.3)");
	$current2.css("background","rgba(194,0,0,1)");//当前元素始终加深
	});
	//鼠标移动判定结束
	$("#body").hide();
	$("#body").slideDown(1000);
</script>
</body>
</html>