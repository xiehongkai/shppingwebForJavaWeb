<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>


<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title></title>
<style>
#ul li {
	color: white;
	margin: 0 1px;
	float: left;
	list-style-type: none;
	height: 100%;
	background: rgba(194, 0, 0, 0.3);
	width: 180px;
	text-align: center;
}

#ul li:hover {
	background: rgba(194, 0, 0, 1);
}

#ul li a {
	color: white;
	margin: 0 1px;
	text-decoration: none;
}

body {
	height: 600px;
	width: 100%;
	background: #f0f0f0;
}

#body {
	margin: 0px auto;
	overflow: visible;
	width: 100%;
	height: 850px;
	z-index: 1;
	position: relative;
	background: rgba(147, 281, 226, 0.2);
}

#body #left {
	width: 15%;
	height: 850px;
	position: absolute;
	float: left;
	background: /*url(img//bgleft.jpg) no-repeat scroll center center;*/
}

#body #bgimg {
	position: absolute;
	z-index: -1;
	width: 100%;
	height: 850px;
	background: red;
	background: url(../img/bg2.png) no-repeat scroll center center;
	background-size: 100%;
}

#top {
	width: 100%;
	height: 120px;
	background: transparent url(../img//log.jpg) no-repeat scroll center
		center;
	overflow: hidden;
	z-index: 1;
}

#sort {
	position: relative;
	width: 100%;
	height: 50px;
	padding: 0 12%;
	margin-bottom: 8;
	background: #f0f0f0;
}

#bottom {
	position: absolute;
	bottom: 0px;
	z-index: 3;
	width: 100%;
}

#bottom img {
	width: 100%;
	boder: 0px;
}

.smart-pink { /*设置了整个表格的属性 */
	float: center;
	margin: 10px auto;
	max-width: 750px; /*整张表的最大宽度 */
	background: rgba(255, 183, 200, 0.7);
	padding: 30px 30px 50px 30px;
	font: 12px Arial, Helvetica, sans-serif;
	color: #666; /*灰黑色
            border-radius: 5px;
            -webkit-border-radius: 5px;  /*为了兼容不同版本的浏览器*/
	-moz-border-radius: 5px; /*为了兼容不同版本的浏览器*/
}

.smart-pink h1 {
	font: 24px "Trebuchet MS", Arial, Helvetica, sans-serif;
	padding: 20px 0px 20px 40px;
	display: block;
	margin: -30px -30px 20px -30px;
	color: #FFF;
	background: rgba(255, 105, 180, 0.5);
	text-shadow: 1px 1px 1px #949494; /*#949494一种灰色*/
	/*属性text-shadow: h-shadow 水平阴影位置 v-shadow 垂直阴影位置 blur模糊的距离 color阴影颜色;*/
	border-radius: 5px 5px 0px 0px;
	-webkit-border-radius: 5px 5px 0px 0px;
	-moz-border-radius: 5px 5px 0px 0px;
	border-bottom: 1px solid; /*没有这一句，就会让 h1和Span在一行*/
}

.smart-pink h1>span { /*说明span是H1 的儿子节点*/
	display: block; /*设置为块状元素 此外还有inline 和 none*/
	font-size: 11px;
	color: #FFF;
}

.smart-pink label {
	display: block;
	margin: 0px 0px 5px;
}

.smart-pink label>span { /*说明span是label 的儿子节点*/
	float: left; /*向左浮动*/
	margin-top: 10px;
	color: #5E5E5E;
}

.smart-pink input[type="text"], input[type="email"], textarea {
	color: #555;
	height: 30px;
	line-height: 15px;
	width: 100%;
	padding: 0px 0px 0px 10px;
	margin-top: 5px;
	border: 1px solid #E5E5E5;
	background: rgba(255, 255, 255, 0.8);
	outline: 0;
	/*是绘制于元素周围的一条线，位于边框边缘的外围，可起到突出元素的作用。*/
	/*属性具有outline-color  outline-style  outline-width*/
	-webkit-box-shadow: inset 1px 1px 2px rgba(238, 238, 238, 0.2);
	box-shadow: inset 1px 1px 2px rgba(238, 238, 238, 0.2);
	font: normal 14px/14px Arial, Helvetica, sans-serif;
}

.smart-pink textarea {
	height: 100px;
	padding-top: 10px;
}

.smart-pink .button {
	width: 80px;
	height: 40px;
	background-color: HotPink;
	border-radius: 5px;
	-webkit-border-radius: 5px;
	-moz-border-border-radius: 5px;
	border: none;
	border-color: red;
	padding: 10px 25px 10px 25px;
	color: #FFF;
	text-shadow: 1px 1px 1px #949494;
}

.smart-pink .button:hover { /*鼠标移动到按钮上面*/
	background-color: #DC143C;
}
</style>
<script src="../js/1.js"></script>
</head>
<body>
	<div id='top'></div>
	<div id='sort'>
		<ul id='ul'>
			<a href='index.jsp'><li><p>网站首页</p></li></a>
			<a href='show_product.jsp'><li><p>产品展示</p></li></a>
			<a href='liuyan.jsp'><li><p>顾客留言</p></li></a>
			<a href='we.jsp'><li><p>关于我们</p></li></a>
			<a href='lianxi.jsp'><li><p>联系我们</p></li></a>
		</ul>
	</div>
	<div id="body">
		<div id='left'></div>
		<div id="bgimg">

			<form action="messageAction!sendMsg" method="post" class="smart-pink">
				<h1>
					留言信息 <span>请留下你的信息.</span>
				</h1>
				<label> <span>姓名 :</span> <input id="name" type="text"
					name="name" class="error" placeholder="请输入您的姓名" />

				</label> <label> <span>邮箱 :</span> <input id="email" type="email"
					name="email" placeholder="请输入邮箱地址" />

				</label> <label> <span>联系地址 :</span> <input id="address" type="text"
					name="address" placeholder="请输入联系地址" />

				</label> <label> <span>留言 :</span> <textarea id="message" name="msg"
						placeholder="请输入你的留言"></textarea>
				</label> <label> <input type="submit" class="button" value="提交" />
				</label>

			</form>
			<div id='bottom'>
				<img src='../img/bottom.jpg'>
			</div>
		</div>

	</div>


	<script type="text/javascript">
		$("#ul li").eq("2").css("background", "rgba(194,0,0,1)");
		$("#body").hide();
		$("#body").slideDown(1000);
		<%
			String err = (String)session.getAttribute("err");
			if (err != null){
				if (err == "sendSu"){
					out.print("alert('留言成功，需通过管理员审核才能显示！')");
				}
			}
		%>
		//单击了第一个按钮
	</script>
</body>
</html>

