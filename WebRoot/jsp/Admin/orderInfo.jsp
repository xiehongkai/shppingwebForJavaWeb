<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page language="java" import="DataBase.DataBase"%>
<%@ page language="java" import="java.sql.*"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>订单状态信息更新</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link rel="stylesheet" href="css/bootstrap3.3.7.css">
<link rel="stylesheet" href="css/bootstrap-theme.min.css">
<link rel="stylesheet" href="css/wcCss.css">
<style>
.update-info {
	padding: 0;
	background-color: #eeeeee;
}

.btn-right {
	float: right;
}

.title {
	text-align: center;
	font-weight: bold;
}

.form-horizontal .form-group {
	margin-right: 0;
}
</style>
</head>

<body>
	<%
		String orderId = (String) session.getAttribute("orderId");
		DataBase database = new DataBase();
		database.setTable("indent");
		ResultSet re = database.queryFromOneoId(orderId);
		if (re != null) {
			try {
				re.next();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	%>
	<div class="container">
		<div class="head">
			<h4>订单详细信息更新</h4>
		</div>
		<div class="container update-info" style="max-width: 50%">
			<form class="form-horizontal" action="indentAction!orderInfo">
				<input type="hidden" name="updateId" value=<%out.print(orderId); %>>
				<h3 class="title">请修改</h3>
				<div class="form-group">
					<label class="col-sm-2 control-label">订单号:</label>
					<div class="col-sm-10">
						<p style="margin-bottom: 0; margin-top: 8px"><%out.print(re.getString("oid")); %></p>
					</div>
				</div>
				<div class="form-group">
					<label for="confirm" class="col-sm-2 control-label">收货状态</label>
					<div class="col-sm-10">
						<!-- 使用下拉列表框不得行 -->
						<select class="form-control" name="confirm" id="SeStatus">
							<%	
								System.out.println(re.getString("confirm"));
								String Wflag=null, Yflag=null;
								if(re.getString("confirm").equals("未收货")){
									Wflag = "selected";
									
								} else {
									Yflag = "selected";
									
								}
							%>
							<option <%
								if(Wflag!=null){
									out.print("selected="+Wflag);
								}
							%> value="未收货">未收货</option>
							<option <%
								if(Yflag!=null){
									out.print("selected="+Yflag);
								}
							%> value="已收货">已收货</option>
						</select>
						<%-- <input type="text" name="confirm" class="form-control" id="confirm"
						required value=<%out.print(re.getString("confirm")); %>> --%>
					</div>
				</div>
				<div class="form-group">
					<label for="send" class="col-sm-2 control-label">发货状态</label>
					<div class="col-sm-10">
						<select class="form-control" name="send" id="SeStatus">
							<%	
								System.out.println(re.getString("send"));
								Wflag=null; Yflag=null;
								if(re.getString("send").equals("未发货")){
									Wflag = "selected";
									
								} else {
									Yflag = "selected";
									
								}
							%>
							<option <%
								if(Wflag!=null){
									out.print("selected="+Wflag);
								}
							%> value="未发货">未发货</option>
							<option <%
								if(Yflag!=null){
									out.print("selected="+Yflag);
								}
							%> value="已发货">已发货</option>
						</select>
						<%-- <input type="text" name="send" class="form-control" id="send"
						required value=<%out.print(re.getString("send")); %>> --%>
						
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-info btn-right">
							<span class="glyphicon glyphicon-refresh"></span>更新
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
<%
	session.removeAttribute("orderId");//清楚session
 %>
<script src="js/jquery-1.12.4.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</html>
