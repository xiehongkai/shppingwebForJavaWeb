<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page language="java" import="DataBase.DataBase"%>
<%@ page language="java" import="java.sql.*"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<title>订单删除</title>
<link rel="stylesheet" href="css/bootstrap3.3.7.css">
<link rel="stylesheet" href="css/bootstrap-theme.min.css">
<link rel="stylesheet" href="css/wcCss.css">
</head>

<body>
	<div class="container">
		<div class="head">
			<h4>订单删除：</h4>
		</div>
		
		<%
			DataBase database = new DataBase();
			database.setTable("Indent");
			ResultSet re = database.query();
		%>
		
		<div class="table-responsive">
			<table class="table table-hover table-bordered">
				<caption class="tHead">订单列表</caption>
				<thead>
					<tr>
						<td>订单编号</td>
						<td>商品名称</td>
						<td>金额</td>
						<td>发货状态</td>
						<td>收货状态</td>
						<td>订单状态</td>
						<td>下单时间</td>
						<td>操作</td>
					</tr>
				</thead>
				
				<% 
					while(re.next()){
						out.print("<td>"+re.getString("oid")+"</td>");
						out.print("<td>"+re.getString("pname")+"</td>");
						out.print("<td>"+re.getDouble("tprice")+"</td>");
						out.print("<td>"+re.getString("send")+"</td>");
						out.print("<td>"+re.getString("confirm")+"</td>");
						out.print("<td>"+re.getString("status")+"</td>");
						out.print("<td>"+re.getDate("cdate")+"</td>");
						out.print("<td><a id='delA' type='button' class='btn btn-danger btn-sm' href='indentAction!delOrder?orderId=" + re.getString("oid") + "'>");
						out.print("<span class='glyphicon glyphicon-trash' aria-hidden='true'></span>");
						out.print("删除</a></td></tr>");
					}
				%>
			</table>
		</div>
		<nav aria-label="Page navigation" style="text-align: center">
		<ul class="pagination">
			<li><a href="#" aria-label="Previous"> <span
					aria-hidden="true">&laquo;</span>
			</a></li>
			<li><a href="#">1</a></li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
			<li><a href="#">4</a></li>
			<li><a href="#">5</a></li>
			<li><a href="#" aria-label="Next"> <span aria-hidden="true">&raquo;</span>
			</a></li>
		</ul>
		</nav>
	</div>
</body>
<script src="js/jquery-1.12.4.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
	$(function(){
		$("#delA").click(function(){
			var message = confirm("确定删除该用户?");
			if (message){
				return true;
			}
			else {
				return false;
			}
		});
	});
</script>
</html>
