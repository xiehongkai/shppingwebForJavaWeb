<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page language="java" import="DataBase.DataBase"%>
<%@ page language="java" import="java.sql.*"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>删除留言</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">

<link rel="stylesheet" href="css/bootstrap3.3.7.css">
<link rel="stylesheet" href="css/bootstrap-theme.min.css">
<link rel="stylesheet" href="css/wcCss.css">

</head>

<body>
	<div class="container">
		<div class="row">
			<div class="head">
				<h4>删除留言</h4>
			</div>
			<%
				DataBase database = new DataBase();
				database.setTable("message");
				ResultSet re = database.query();
			%>
			
			<div class="table-responsive">
				<table class="table table-hover table-bordered">
					<caption class="tHead">现有留言信息</caption>
					<thead>
						<tr>
							<td>ID</td>
							<td>姓名</td>
							<td>邮箱</td>
							<td>留言内容</td>
							<td>留言状态</td>
							<td>操作</td>
						</tr>
					</thead>
					
					<% 
						while(re.next()){
							out.print("<td>"+re.getInt("id")+"</td>");
							out.print("<td>"+re.getString("name")+"</td>");
							out.print("<td>"+re.getString("email")+"</td>");
							out.print("<td>"+re.getString("msg")+"</td>");
							out.print("<td>"+re.getString("status")+"</td>");
							out.print("<td><a id='delA' type='button' class='btn btn-danger btn-sm'");
							out.print("href='messageAction!delMessage?delId=" + re.getInt("id") + "'>"); 
							out.print("<span class='glyphicon glyphicon-trash' aria-hidden='true'></span>删除</a></td>");
							out.print("</tr>");
						}
					%>
				</table>
			</div>
			<nav aria-label="Page navigation" style="text-align: center">
			<ul class="pagination">
				<li><a href="#" aria-label="Previous"> <span
						aria-hidden="true">&laquo;</span>
				</a></li>
				<li><a href="#">1</a></li>
				<li><a href="#">2</a></li>
				<li><a href="#">3</a></li>
				<li><a href="#">4</a></li>
				<li><a href="#">5</a></li>
				<li><a href="#" aria-label="Next"> <span aria-hidden="true">&raquo;</span>
				</a></li>
			</ul>
			</nav>
		</div>
	</div>
</body>
<script src="js/jquery-1.12.4.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
	$(function(){
		$("#delA").click(function(){
			var message = confirm("确定删除该用户?");
			if (message){
				return true;
			}
			else {
				return false;
			}
		});
	});
</script>
</html>
