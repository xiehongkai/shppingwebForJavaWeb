<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page language="java" import="DataBase.DataBase"%>
<%@ page language="java" import="java.sql.*"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//zh">
<html>
<head>
<base href="<%=basePath%>">

<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<title>留言状态信息更新</title>
<link rel="stylesheet" href="css/bootstrap3.3.7.css">
<link rel="stylesheet" href="css/bootstrap-theme.min.css">
<link rel="stylesheet" href="css/wcCss.css">
<style>
.update-info {
	padding: 0;
	background-color: #eeeeee;
}

.btn-right {
	float: right;
}

.title {
	text-align: center;
	font-weight: bold;
}

.form-horizontal .form-group {
	margin-right: 0;
}
</style>
</head>
<body>
	<div class="container">
		<div class="head">
			<h4>留言状态信息更新</h4>
		</div>

		<%
			String updateId = (String) session.getAttribute("messageId");
			DataBase database = new DataBase();
			database.setTable("message");
			ResultSet re = database.queryFromOne(updateId);

			if (re != null) {
				try {
					re.next();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		%>

		<div class="container update-info" style="max-width: 50%">
			<form class="form-horizontal" action="messageAction!updateInfo" method="post">
				<input type="hidden" name="updateId" value="<%out.print(re.getInt("id")); %>" />
				<h3 class="title">请修改</h3>
				<div class="form-group">
					<label class="col-sm-2 control-label">留言者</label>
					<div class="col-sm-10">
						<p style="margin-bottom: 0; margin-top: 8px">
							<%
								out.print(re.getString("name"));
							%>
						</p>
					</div>
					<label class="col-sm-2 control-label">留言内容</label>
					<div class="col-sm-10">
						<p style="margin-bottom: 0; margin-top: 8px">
							<%
								out.print(re.getString("msg"));
							%>
						</p>
					</div>
				</div>
				<div class="form-group">
					<label for="Status" class="col-sm-2 control-label">留言状态</label>
					<div class="col-sm-10">
						<select class="form-control" name="status" id="Status">
							<%
								String flag1 = null, flag2 = null;
								if (re.getBoolean("status")) {
									flag1 = "selected";
								} else {
									flag2 = "selected";
								}
							%>
							<option 
							<%
								if (flag1 != null)
									out.print(flag1);
							%> value='true'>已审核</option>
							<option 
							<%
								if (flag2 != null)
									out.print(flag2);
							%> value='false'>未审核</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-info btn-right">
							<span class="glyphicon glyphicon-refresh"></span>更新
						</button>
					</div>
				</div>
			</form>
		</div>

	</div>
</body>
<script src="js/jquery-1.12.4.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</html>
