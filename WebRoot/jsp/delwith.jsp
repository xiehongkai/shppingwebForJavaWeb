<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="DataBase.DataBase" %>
<%@ page import="java.sql.ResultSet"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>确认购买</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
  	<style>
	#left
	{
		position:absolute;
		top:20px;
		left:20px;
		width:510px;
		height:500px;
		background:#f0f0f0;
	}
	#left img
	{
		width:500px;
		height:500px;
	}
	#right
	{
		position:absolute;
		top:20px;
		left:530px;
		width:500px;
		height:500px;
		background:pink;
	}
	#right img
	{
		width:200px;
		height:200px;
		position:absolute;
		top:80px;
		left:285px;
	}
</style>
  </head>
  <body>
    	<%
    		//这是付款页面
    		String buy=request.getParameter("buy");
    		/*如果是地址栏直接如数，就跳转到404页面 */
    		if(buy==null)response.sendRedirect("jsp/404.jsp");
    		
    		String pid = request.getParameter("pid");
    		String number = request.getParameter("number");
    		String size = request.getParameter("size");
    		String color = request.getParameter("color");
    		String pname =request.getParameter("pname");
    		double uprice=Double.parseDouble(request.getParameter("uprice"));
    		double tprice =Integer.parseInt(number)*uprice;
    	 %>
    	 <div id='left'>
		<img src="<%="img/pro/pro2/"+pid+"/1.jpg"%>"	/>	
	</div>
	<div id='right'>
		<div id='in_right'>
			<p>商品名：<a href="jsp/info.jsp?id=<%=pid%>"><%=pname %></a></p>
			<p>单价：<%=uprice %></p>
			<pre>数量：<%=number %>           颜色:<%=color %>      尺寸：<%=size %></pre>
			<p style="margin-right:10px;">待支付:<%=tprice %></p>
			<form name='frm_buy_information' action='indentAction!submitIndent' method='post'>
				<p>收件人姓名:<input type='search' name='rname' placeholder="姓名不超过6位" autofocus required maxlength="6"></p>
				<p>收货地址:<textarea  style="resize:none;overflow:hidden;" rows='3' cols='20' name='address' required></textarea></p>
				<p>联系电话:<input type='number'  name='phonenumber' required placeholder="电话为8到15位" min='00000000' max='999999999999999'></p>
				<p>支付订单号:<input type='number' name='paymentNumber' required  placeholder="订单号后8位" min='00000000' max='999999999'>
				<!-- 隐藏域保存商品需要自动传入后台处理的参数 -->
				<input type='hidden' name='pid' value="<%=pid%>">
				<input type='hidden' name='pname' value="<%=pname%>">
				<input type='hidden' name='size' value="<%=size%>">
				<input type='hidden' name='color' value="<%=pid%>">
				<input type='hidden' name='number' value="<%=number%>">
				<input type='hidden' name='uprice' value="<%=uprice%>">
				<input type='hidden' name='tprice' value="<%=tprice%>">
				
				<p><input type='submit' name='log_order' value='提交订单' style="width:270px;height:50px;background:orange;"></p>
			</form>
		</div>
		<img src='img/fk.jpg'><!-- 支付宝付款码-->
	</div>
  </body>
</html>
