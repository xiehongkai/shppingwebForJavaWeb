<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="DataBase.DataBase" %>
<%@ page import="java.sql.ResultSet"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
	<title>我的简易购物网站</title>
	<meta charset='utf-8'>
<style>
	body
	{
		height:2000px;
		width:100%;
		background:#f0f0f0;
	}
	#top
	{
		width:100%;
		height:120px;
		background:transparent url(img//log.jpg) no-repeat scroll center center;
		overflow:hidden;
		z-index:1;
	}
	#sort
	{
		position:relative;
		width:100%;
		height:50px;
		background:#f0f0f0;
		padding:0 12%;
		margin-bottom:8;
	}
	#ul li
	{	
		color:white;
		margin:0 1px;
		float :left;
		list-style-type:none;
		height:100%;
		background:rgba(194,0,0,0.3);
		width:180px;
		text-align:center;
	}
	#ul li a
	{
		color:white;
		margin:0 1px;
		text-decoration:none;
	}
	#body
	{	
		overflow:visible;
		width:100%;
		height:850px;
		z-index:1;
		position:relative;
		background:rgba(147,281,226,0.2);
	}
	#body #left
	{
		width:15%;
		height:900px;
		position:relative;
		float:left;
		background:
		url(img//bgleft.jpg) no-repeat scroll center center;
	}
	#left #div1
	{
		width:100%;
		height:20%;
		background:rgba(31,31,31,0.9);
	}
	#left #div2
	{
		width:100%;
		height:79.9%;
		background:rgba(166,166,166,0.6);
		position:absolute;
		top:20.1%;
	}
	#body #bgimg
	{
		width:70%;
		height:1080px;
		z-index:1;
		background:
		url(img//bg2.png) no-repeat scroll center center;
		position:relative;
		background-attachment:scroll;
		float:left;
	}
	#bgimg #divblack
	{
		z-index:2;
		width:100%;
		height:900px;
		background:rgba(21,21,21,0.7);
		display:none;
		
	}
	#body #right
	{
		width:15%;
		height:100%;
		position:relative;
		float:right;
	}
	#bottom
	{
		margin:10 auto;
		width:100%;
		height:600px;
		background:#f0f0f0;
		position:absolute;
		top:1030;
	}
	input[type='submit']
	{
		background:pink;
		color:black;
		margin:20px 25px;
		border-radius:50px;
	}
	#right #tx
	{
		width:100px;
		margin:10px auto;
		height:100px;
		border:1px solid blue;
		border-radius:50px;
		background: url(img/tx.png);
	}
	#right #right_2
	{
		margin:5 auto;
		padding:5 0;
	}
	#bgimg li
	{
		z-index:9;
		width:250px;
		height:280px;
		background:white;
		margin-left:10px;
		list-style-type:none;
		float:left;
		margin-bottom:10px;
	}
	#bgimg #in_div
	{
		width:100%;
		position:absolute;
		top:10px;
		left:20px;
		display:none;
	}
	#in_div a
	{
		margin:0;
		padding:0;
		text-decoration: none;
	}
}
</style>
<script src="./js/1.js"></script>
</head>
<body>
	<div id='top'></div>
	<div id='sort'>
		<ul id='ul'>
			<a href='./jsp/index.jsp'><li><p>网站首页</p></li></a>
			<a href='./jsp/show_product.jsp'><li><p>产品展示</p></li></a>
			<a href='./liuyan.jsp'><li><p>顾客留言</p></li></a>
			<a href='./we.jsp'><li><p>关于我们</p></li></a>
			<a href='./lianxi.jsp'><li><p>联系我们</p></li></a>
		</ul>
	</div>
	<div id='body'>
		<div id='left'>
			<div id='div1'></div>
			<div id='div2'></div>
		</div>
		<div id='bgimg'>
			<!-- 中间区域透明黑色背景-->
			<div id='divblack'></div>
				<%
					String flag=request.getParameter("flag");
					if(flag!=null)//如果是通过别的页面进入的，有flag
					{	
						DataBase database =new DataBase();
						switch(Integer.parseInt(flag))
							 {
								 case 1:database.setTable("hot");break;
								 case 2:database.setTable("new");break;
								 case 3:database.setTable("dress");break;
								 case 4:database.setTable("tshirt");break;
								 case 5:database.setTable("leisi");break;
								 case 6:database.setTable("double_tao");break;
								 case 7:database.setTable("halfdress");break;
								 case 8:database.setTable("trousers");break;
							 }
							 //产品分类区域选择判断完毕
						ResultSet re=database.query();
						out.println("<div id='in_div'>");
						out.println("<ul>");
						if(re!=null)
						{
								while(re.next())
							{
							//单击显示的页面
								int pid=re.getInt("id");//获取商品id
								String pname=re.getString("name");//获取商品名称
								int scount =re.getInt("scount");//获取商品数量
								String price=re.getString("price");//获取商品价格
								String src="./img/pro/pro2/"+pid;
								out.println("<li><a href='productAction!proInfo?id="+pid+"' target='_blank'>");
								//<!-- 大图区域 title=空是为了属性选择器加动画的-->
								out.println("<img src='"+src+"/1.jpg' width='250' height='140' title="+pid+" class='bigimg'>");
								//小图区域
								out.println("<img src='"+src+"/1.jpg' width='64' height='64' style='margin:5 5'>");
								out.println("<img src='"+src+"/2.jpg' width='64' height='64' style='margin:5 5'>");
								out.println("<img src='"+src+"/3.jpg' width='64' height='64' style='margin:5 5'>");
								//价格区域
								out.println("<br/>");
								out.println("<span style='color:rgb(255,155,0);font-weight:bold;'>￥"+price+"</span>");
								out.println("<br/>");
								//商品名区域
								if(pname.length()>=15)
									pname=pname.substring(0, 15);
								out.println("<span style='font-size:12;letter-spacing:2;margin:5 5;'>"+pname+"</span><br/>");
								out.println("<span style='color:rgb(153,153,153);display:block;margin:5;'>30天成交:"+scount+"件</span>");
								out.println("</li></a>");
							}//endwhile
							out.println("</ul>");
							out.println("</div>");
						}//endif re!=null
					}//endif flag!=null
				else
				{
					//response.sendRedirect("userAction");
					}
				 %>
		</div>
		<div id='right'>
			<div id='tx'>
			</div>
			
			<div id="right_2">
				<?php
					if(isset($_GET['type']))
						echo "hahaha";
				?>
				<form name='frm'>
				<tr>
				<td><input type="submit" name='log_sub' formaction='handle.php' value='登录'></td>
				<td><input type="submit" name='reg_sub' formaction='handle.php' value='注册'></td>
				</tr>
				<form>
			</div>
		</div>
	</div>
	<div id='bottom'>
		<!--<center><p><embed src='mp4//衣锦年华女装店宣传视频.mp4' width='800' 
		height='100%' ></embed></p></center> -->
		
	</div>
<script type="text/javascript">
	 //鼠标移动判定开始
	$current = $("#sort li:eq(1)"); //匹配第二个列表元素
	$current.css("background","rgba(194,0,0,1)");
	$("#sort li").click(function(){
		$("#sort li").css("background","rgba(194,0,0,0.3)");//淡色所有元素
		$(this).css("background","rgba(194,0,0,1)");//加深当前元素
		$current = $(this);//记录当前元素
	});
	$("#sort li").hover(function(){
	$(this).css("background","rgba(194,0,0,1)");
	},function(){
	$(this).css("background","rgba(194,0,0,0.3)");
	$current.css("background","rgba(194,0,0,1)");//当前元素始终加深
	});
	//鼠标移动判定结束
	
	//div元素鼠标动画判定开始
	$("#in_div li").hover(function(){
		$(this).css("border","4px solid rgb(255,211,0)")
	},function(){
		$(this).css("border","0px");
	});
	//div元素鼠标动画判定结束
	
	$("#in_div li").css("position","absolute");
	
	//设置 #in_div li中的每一个元素绝对定位，防止动画执行时位置变化
	//jquery 版
   $("#in_div li").each(function(index,element){
		var top,left;
		 if(index%9<3)
			 top = '10px';
		 else if(index%9<6)
			   top = '300px';
		 else  top ='590px';
		 if(index%3==0)
			 left = '20px';
		 else if(index%3==1)
			 left = '280px';
		 else left ='540px';
		 
		 $(element).css("top",top);
		 $(element).css("left",left);
		 $(element).on('click',function()
		 {	
			$img = $(".bigimg:eq("+index+")");
			$pro_id = $img.attr('title');
		 });
});
	//js版
	/* var list = document.getElementById("in_div").
	getElementsByTagName("ul")[0].getElementsByTagName("li");
	//alert(list.length);
	var l,t;
	for(var index = 0; index < list.length; index++){
		 if(index%9<3)
			 t = "10px";
		 else if(index%9<6)
			   t = '300px';
		 else  t ='590px';
		 if(index%3==0)
			 l = '20px';
		 else if(index%3==1)
			 l = '280px';
		 else l ='540px';
		 
		 list[index].style['top'] = t;
		 list[index].style['left'] = l;
	} */
	//绝对定位设置完毕
	
	$("#bgimg div").slideToggle(1000);
</script>
<script type="text/javascript">  
    $(function(){  
        $("[title='*']").mouseenter(function(){  
			var w = $(this).width();
			var h =$(this).height();
            var wValue=1.01 * w;
            var hValue=1.01 * h;       
            $(this).animate({width: wValue,  
                            height: hValue,  
                            left:("-"+(0.5 * $(this).width())/2),  
                            top:("-"+(0.5 * $(this).height())/2)}, 200);  
        }).mouseleave(function(){  
            $(this).animate({width: "250",  
                                         height: "140",  
                                         left:"0px",  
                                         top:"0px"}, 200 );  
        });  
    });  //移入图片放大
</script>  
</body>
</html>
