<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<html>
<head>
	<!--设置本文档的路劲为  -->
	<base href="<%=basePath%>">
	<meta charset='utf-8'/><title>我的简易购物网站</title>
	<link href="./css/index.css" rel='stylesheet' type='text/css'/>
</head>
<script src="./js/1.js"></script>
<!-- 碎片化效果 -->
<script src='./js/suipian.js'></script>
<body>
	<div id='top'></div>
	<div id='sort'>
		<ul id='ul'>
			<a href=''><li><p>网站首页</p></li></a>
			<a href='./jsp/show_product.jsp'><li><p>产品展示</p></li></a>
			<a href='./jsp/liuyan.jsp'><li><p>顾客留言</p></li></a>
			<a href='./jsp/we.jsp'><li><p>关于我们</p></li></a>
			<a href='./jsp/lianxi.jsp'><li><p>联系我们</p></li></a>
		</ul>
	</div>
	<div id='body'>
		<div id='left'>
			<video style='position:relative;' width='195' autoplay='autoplay' controls='controls' preload='preload' src='./mp4/2.mp4'></video>
		</div>
		<div id='center'>
			<ul>
				<li class="selected" flag="1"><a href=><img src="./img/advertice/1.jpg"></img></a></li>
				<li flag="0"><a href=><a href=><img src="./img/advertice/2.jpg"></img></a></li>
				<li flag="0"><a href=><a href=><img src="./img/advertice/3.jpg"></img></a></li>
				<li flag="0"><a href=><a href=><img src="./img/advertice/4.jpg"></img></a></li>
			</ul>
			<div id="btn_left"></div>
			<div id="btn_right"></div>
			<ol>
				<li class='selected' index="0">1</li>
				<li index='1'>2</li>
				<li index='2'>3</li>
				<li index='3'>4</li>

			</ol>
		</div>
		<div id='right'>
			<%
				String userid =(String)session.getAttribute("userid");
				if(userid!=null)
				{
					String uname = (String)session.getAttribute("username");
					out.println("<div id='tx'></div>");
					out.println("<center>Welcome <a href='userAction!userinfo'>"+uname+"</a><br/>");
					out.println("<a href='userAction!logout'>登出</a></center>");
				}
				else//如果没有登录就出现登录注册按钮
				{
					out.println("<div id='tx'></div>");
					out.println("<div id='right_2'>");
					out.println("<table>");
					out.println("<tr>");
					out.println("<table>");
					out.println("<td><input type='submit' name='log_sub' formaction='handle.jsp' value='登录'></td>");
					out.println("<td><input type='submit' name='reg_sub' formaction='handle.jsp' value='注册'></td>");
					out.println("</tr></table>");
					out.println("</div>");
				}
			%>
		</div>
	</div>
	<div id="sx">
		<a href='./js/404.jsp'><img src="./img/index/sx.png"></a>
	</div>
	<div id="mysort">
		<img src="./img/index/mysort.png">
	</div>
	<div id="friday">
		<img src="./img/index/friday.png">
	</div>
	<div id="camera">
		<a href='./jsp/camera.jsp' target="_bank">
			<img src="./img/index/camera.png">
		</a>
	</div>
	<div id="joinus">
		<img src="./img/index/joinus.png">
	</div>
<div id='bottom'>
</div>
<script	type="text/javascript">
	var uli =$("#center ul li");//获取广告图
	var oli =$("#center ol li");//获取广告图的小圆球元素
	var index=0;//记录第几张轮播图
	var left = $("#btn_left");
	var right = $("#btn_right");
	function myanimation(t)//要显示的动画//未完成，未用到（动画逻辑未设计）
	{
		t.animate(
		{
			width:"100%",
			height:"906px",
			display:"block"
		},500);
	}
	oli.click(function(){
		oli.eq(index).removeClass("selected");//清除当前元素的选中状态
		uli.eq(index).hide();
		//获取当前单机的元素的index属性值
		index=$(this).attr("index");
		//uli.eq(index).slideToggle(100,"linear");
		//淡入这张广告
		uli.eq(index).fadeToggle(1000,"linear");
		oli.eq(index).addClass("selected");//显示这张广告
		
	});
	left.click(function(){
		//uli.eq(index).removeClass("selected");//清除当前元素的选中状态
		uli.eq(index).hide();
		oli.eq(index).removeClass("selected");//清除当前元素的选中状态
		uli.eq(index).attr("flag","0");//flag标致改为0
		index--;//切换上一张广告
		if(index<0)
			index=3;//index不能小于0,4张图的话，下标0-3
		//uli.eq(index).addClass("selected");//显示这张广告
		uli.eq(index).slideToggle(100,"linear")
		//myanimation(uli.eq(index));
		oli.eq(index).addClass("selected");//小圆球变化
		uli.eq(index).attr("flag","1");//当前广告flag=1;
	});
	right.click(function(){
		//uli.eq(index).removeClass("selected");//清除当前元素的选中状态
		uli.eq(index).hide();
		oli.eq(index).removeClass("selected");//清除当前圆球元素的选中状态
		uli.eq(index).attr("flag","0");//flag标致改为0
		index++;//切换下一张广告
		if(index>3)
			index=0;//index不能大于30,4张图的话，下标0-3
		//uli.eq(index).addClass("selected");//显示这张广告
		uli.eq(index).slideToggle(100,"linear")
		oli.eq(index).addClass("selected");//显示这张广告
		uli.eq(index).attr("flag","1");//当前广告flag=1;
	})
	//轮播图广告三秒切一次
	setInterval(function(){
		uli.eq(index).hide();
		oli.eq(index).removeClass("selected");//清除当前圆球元素的选中状态
		uli.eq(index).attr("flag","0");//flag标致改为0
		index++;//切换下一张广告
		if(index>3)
			index=0;//index不能大于30,4张图的话，下标0-3
		//uli.eq(index).addClass("selected");//显示这张广告
		uli.eq(index).slideToggle(100,"linear")
		oli.eq(index).addClass("selected");//显示这张广告
		uli.eq(index).attr("flag","1");//当前广告flag=1;
	}, 3000);
	
</script>
<script type="text/javascript">
	
	
	//鼠标移动判定开始
	$current = $("#sort li:first"); //匹配第一个列表元素
	$current.css("background","rgba(194,0,0,1)");
	$("#sort li").click(function(){
		$("#sort li").css("background","rgba(194,0,0,0.3)");//淡色所有元素
		$(this).css("background","rgba(194,0,0,1)");//加深当前元素
		$current = $(this);//记录当前元素
	});
	$("#sort li").hover(function(){
	$(this).css("background","rgba(194,0,0,1)");
	},function(){
	$(this).css("background","rgba(194,0,0,0.3)");
	$current.css("background","rgba(194,0,0,1)");//当前元素始终加深
	});
	//鼠标移动判定结束
	
	//登录注册判定
	function create_Sign() //创建登录框函数
	{
		$ht ="<div id='img'></div>"+
				"<div id='sign_kuang'>"+
					"<center><p>请登录</p></center>"+
					  "<form name='frm' action='userAction!login' method='post'>"+
						"<div id='img1'><img  src='./img/sign/user.png'><input type='search' maxlength='16' placeholder='Enter username' name='username' autofocus required></div>"+
						"<div id='img2'><img  src='./img/sign/psw.png'><input type='password' maxlength='16' placeholder='Enter userpassword' name='password' required></div>"+
						"<input id='btn_sub' type='submit' name='log_sub' value='登录'>"+
					"</form>"+
				"</div>";
			  
		$ht=$($ht);
		$("#body").append($ht);
		suipian();//执行碎片动画

	}//函数在这里结束
	
	function create_Reg() //创建注册框函数
	{
		$ht ="<div id='img'></div>"+
				"<div id='reg_kuang'>"+
					"<center><p>欢迎注册</p></center>"+
					  "<form name='frm' action='userAction!regist' method='post'>"+
						"<div id='img1'><img  src='./img/sign/user.png'><input type='search' placeholder='用户名长度在6-16,数字字母,下划线' name='username' autofocus required maxlength='16'></div>"+
						"<div id='img2'><img  src='./img/sign/psw.png'><input type='password' placeholder='密码长度6-16' name='password' required maxlength='16'></div>"+
						"<input id='btn_sub' type='submit' name='reg_sub' value='提交'>"+
					  "</form>"+
				"</div>";
			  
		$ht=$($ht);
		$("#body").append($ht);
	}//函数在这里结束
	
	$("#right :submit:first").click(function(){
		$("#body").empty();//清空界面
		create_Sign();//创建 登陆狂
	})
	$("#right :submit:last").click(function(){
		$("#body").empty();//清空界面
		create_Reg();//创建注册框
		suipian();//执行碎片动画
	})
	//登录注册判定结束
	
	//错误处理页面状态吗处理界面
	<%
		String err = (String)session.getAttribute("err");
		if(err!=null)//登录失败的状态码判定
		{
			if(err.equals("1"))//未登录单机了购买或加入购物车
			{
				out.println("alert('请先登录')"+";");
				out.println("$('#body').empty()"+";");//消除界面主题
				out.println("create_Sign()"+";");//出现登录框
			}
			else if(err.equals("801"))//801代表用户名或密码错误
			{
				out.println("alert('用户名或密码错误请重试')"+";");
					out.println("$('#body').empty()"+";");//消除界面主题
				out.println("create_Sign()"+";");//出现登录框
			}
			else if(err.equals("903"))//错误码903代表用户名长度错误
			{
				out.println("alert('用户名长度应该在6-16,请重试')"+";");
					out.println("$('#body').empty()"+";");//消除界面主题
				out.println("create_Reg()"+";");//注册框
			}
			else if(err.equals("904"))//错误码904代表密码长度错误
			{
				out.println("alert('密码长度应该在6-16,请重试')"+";");
					out.println("$('#body').empty()"+";");//消除界面主题
				out.println("create_Reg()"+";");//注册框
			}
			else if(err.equals("803"))//错误码803代表用户名有其它字符
			{
				out.println("alert('用户名只能包含数字，字母下划线')"+";");
					out.println("$('#body').empty()"+";");//消除界面主题
				out.println("create_Reg()"+";");//注册框
			}
			else if(err.equals("900"))//错误码900代表用户名已经注册过了
			{
				out.println("alert('用户名已经被注册过了')"+";");
					out.println("$('#body').empty()"+";");//消除界面主题
				out.println("create_Reg()"+";");//注册框
			}
			else if(err.equals("suc"))//注册成功
			{
				//if
				out.println("if(confirm('注册成功,现在马上登陆么'))");
				out.println("{");
				out.println("$('#body').empty();");//消除界面主题
				out.println("create_Sign();");//出现登陆狂
				out.println("}");
				//esle
				out.println("else{");
				out.println("$('#body').empty();");//消除界面主题
				out.println("create_Reg();");//出现注册框
				out.println("}");
			}
			session.removeAttribute("err");//清楚err状态吗
		}//endif;
		
	%>
</script>
</body>
</html>
