<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page language="java" import="DataBase.DataBase"%>
<%@ page language="java" import="java.sql.*"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>用户增加</title>
<link rel="stylesheet" href="css/bootstrap3.3.7.css">
<link rel="stylesheet" href="css/bootstrap-theme.min.css">
<link rel="stylesheet" href="css/wcCss.css">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<style>
.update-info {
	padding: 0;
	background-color: #eeeeee;
}

.btn-right {
	float: right;
}

.title {
	text-align: center;
	font-weight: bold;
}

.form-horizontal .form-group {
	margin-right: 0;
}
</style>

</head>
<body>
			<% 
          	String userid =(String)session.getAttribute("userid");
          	DataBase database = new DataBase();
          	ResultSet re = database.queryFromOne(userid);
          	if (re != null){
           	try {
				re.next();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
          	}
           %>
	<div class="container">
		<div class="head">
			<h4>用户详细信息更新</h4>
		</div>
		<div class="container update-info" style="max-width: 50%">
			<form class="form-horizontal" action='userAction!updateInfo'>
				<input type="hidden" name="updateId" value=<%out.print(userid); %>>
				<h3 class="title">请修改</h3>
				<div></div>
				<div class="form-group">
					<label for="username" class="col-sm-2 control-label">姓名</label>
					<div class="col-sm-10">
						<input type="text" name="username" class="form-control"
							id="username" placeholder="姓名" required
							value=<%out.print(re.getString("name")); %>>
					</div>
				</div>
				<div class="form-group">
					<label for="email" class="col-sm-2 control-label">邮箱</label>
					<div class="col-sm-10">
						<input type="email" name="email" class="form-control" id="email"
							placeholder="邮箱" required
							value=<%out.print(re.getString("email")); %>>
					</div>
				</div>
				<div class="form-group">
					<label for="phone" class="col-sm-2 control-label">联系电话</label>
					<div class="col-sm-10">
						<input type="text" name="phone" class="form-control" id="phone"
							placeholder="联系电话" required
							value=<%out.print(re.getString("phone")); %>>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-info btn-right">
							<span class="glyphicon glyphicon-refresh"></span>更新
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
<%
	session.removeAttribute("userid");//清除err状态吗
 %>
<script src="js/jquery-1.12.4.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
	<%
		String err = (String)session.getAttribute("err");
		if (err != null){
			if(err.equals("updateSuccess")){
				out.print("alert('恭喜你更新用户成功！')");
			}
			else if(err.equals("updateError")){
				out.print("alert('更新失败！出现异常')");
			}
			session.removeAttribute("err");//清除err状态吗
		}
	%>
</script>
</html>
