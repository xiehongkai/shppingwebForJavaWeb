<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<html>
<head>
	<meta charset='utf-8'>
	<title>我的简易购物网站</title>
</head>
<style>
	body
	{
		height:600px;
		width:1366px;
		background:#f0f0f0;
	}
	#top
	{
		width:100%;
		height:120px;
		background:transparent url("../img//log.jpg") no-repeat scroll center center;
		overflow:hidden;
		z-index:1;
	}
	#sort
	{
		position:relative;
		width:100%;
		height:50px;
		background:#f0f0f0;
		padding:0 12%;
		margin-bottom:8;
	}
	#ul li
	{	
		color:white;
		margin:0 1px;
		float :left;
		list-style-type:none;
		height:100%;
		background:rgba(194,0,0,0.3);
		width:180px;
		text-align:center;
	}
	#ul li:hover
	{
		background:rgba(194,0,0,1);
		font-weight:bold;
		animation:myrotate 6s;
	}
	@keyframes myrotate
	{
		0% {bacbackground:rgba(194,0,0,0.2);left:0px;top:0px;}
		25% {bacbackground:rgba(194,0,0,0.5);left:30px;top:30px;}
		50% {bacbackground:rgba(194,0,0,0.7);left:50px;top:50px;}
		75% {bacbackground:rgba(194,0,0,0.9);left:30px;top:30px;}
		100% {bacbackground:rgba(194,0,0,1);left:0px;top:50px;}
	}
	#ul li a
	{
		color:white;
		margin:0 1px;
		text-decoration:none;
	}
	#body
	{	
		overflow:visible;
		width:100%;
		height:650px;
		z-index:1;
		position:relative;
		background:rgb(239,239,239);
	}
	#body img
	{
		width:885px;
		height:432px;
		position:absolute;
		top:0px;
		left:20%;
		z-index:2;
	}
	#body p
	{
		position:absolute;
		top:0px;
		left:22%;
		z-index:3;
	}
	#bottom
	{
		position:absolute;
		top:630px;
		z-index:3;
	}

}
</style>
<script src="../js/1.js"></script>
<body>
	<div id='top'></div>
	<div id='sort'>
		<ul id='ul'>
			<a href='index.jsp'><li><p>网站首页</p></li></a>
			<a href='show_product.jsp'><li><p>产品展示</p></li></a>
			<a href='liuyan.jsp'><li><p>顾客留言</p></li></a>
			<a href='we.jsp'><li><p>关于我们</p></li></a>
			<a href='lianxi.jsp'><li><p>联系我们</p></li></a>
		</ul>
	</div>
	<div id='body'>
		<div id='body_left'>
		</div>
		<div id='body_center'>
			<p>
				你当前的位置 >> <a href='index.jsp'>网站首页</a> >>
				<a href='we.jsp'>关于我们</a> >>  <a href='lianxi.jsp'>联系我们</a>
			</p>
			<img src='../img/lianxi.jpg'>
		</div>
	</div>
	<div id='bottom'>
		<img src='../img/bottom.jpg'>
	</div>
	
<script type="text/javascript">
	$("li:last").css("background","rgba(194,0,0,1)");//加深当前元素
	$("#body").hide();
	$("#body").slideDown(1000);
</script>
</body>
</html>