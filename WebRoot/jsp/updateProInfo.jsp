<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page language="java" import="DataBase.DataBase"%>
<%@ page language="java" import="java.sql.*"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>用户增加</title>
<link rel="stylesheet" href="css/bootstrap3.3.7.css">
<link rel="stylesheet" href="css/bootstrap-theme.min.css">
<link rel="stylesheet" href="css/wcCss.css">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<style>
.update-info {
	padding: 0;
	background-color: #eeeeee;
}

.btn-right {
	float: right;
}

.title {
	text-align: center;
	font-weight: bold;
}

.form-horizontal .form-group {
	margin-right: 0;
}
</style>

</head>
<body>
			<% 
          	String proid =(String)session.getAttribute("proid");
          	DataBase database = new DataBase();
          	database.setTable("product");
          	ResultSet re = database.queryFromOne(proid);
          	if (re != null){
           	try {
				re.next();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
          	}
           %>
	<div class="container">
		<div class="head">
			<h4>商品详细信息更新</h4>
		</div>
		<div class="container update-info" style="max-width: 50%">
			<form class="form-horizontal" action='productAction!updateInfo'>
				<input type="hidden" name="updateId" value=<%out.print(proid); %>>
				<h3 class="title">请修改</h3>
				<div></div>
				<div class="form-group">
					<label for="name" class="col-sm-2 control-label">商品名称</label>
					<div class="col-sm-10">
						<input type="text" name="name" class="form-control"
							id="name" placeholder="商品名称" required
							value=<%out.print(re.getString("name")); %>>
					</div>
				</div>
				<div class="form-group">
					<label for="scount" class="col-sm-2 control-label">数量</label>
					<div class="col-sm-10">
						<input type="text" name="scount" class="form-control" id="scount"
							placeholder="数量" required
							value=<%out.print(re.getString("scount")); %>>
					</div>
				</div>
				<div class="form-group">
					<label for="price" class="col-sm-2 control-label">价格</label>
					<div class="col-sm-10">
						<input type="text" name="price" class="form-control" id="price"
							placeholder="价格" required
							value=<%out.print(re.getString("price")); %>>
					</div>
				</div>
				<div class="form-group">
					<label for="url" class="col-sm-2 control-label">URL</label>
					<div class="col-sm-10">
						<input type="text" name="url" class="form-control" id="url"
							placeholder="价格" required
							value=<%out.print(re.getString("url")); %>>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-info btn-right">
							<span class="glyphicon glyphicon-refresh"></span>更新
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
<%
	session.removeAttribute("proid");//清除err状态吗
 %>
<script src="js/jquery-1.12.4.min.js"></script>
<script src="js/bootstrap.min.js"></script>

</html>
