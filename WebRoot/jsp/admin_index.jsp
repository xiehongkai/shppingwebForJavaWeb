<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page language="java" import="DataBase.DataBase" %>
<%@ page language="java" import="java.sql.*" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>"> 
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>衣锦年华后台管理</title>
    <link rel="stylesheet" href="css/bootstrap3.3.7.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="css/tree.css">
    <link rel="stylesheet" href="css/wcCss.css">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	
  </head>
  <body>
   <div class="head">
    欢迎管理员：<em class="amdin-red">
    <%-- 显示管理员name --%>
       		<%
       			String userid =(String)session.getAttribute("userid");
				if(userid != null)
				{
					String uname = (String)session.getAttribute("username");
					out.print(uname);
				}
				else {
					out.print("请登录");
				}
       		 %>
    </em>！当前时间是：<span id="curr_time"></span>
</div>
<div class="panel panel-info">
    <div class="panel-heading">
        <h3 class="panel-title">数据统计</h3>
    </div>
    <div class="panel-body">
        <ul>
            <li class="data-li">
                <h4>用户数</h4>
                <p class="li-p hidden-xs">
					<%
						DataBase User = new DataBase();
						User.setTable("user");
						int Usernum = User.getNumber();
						out.print(Usernum);
					%>
				</p>
            </li>
            <li class="data-li">
                <h4>商品数</h4>
                <p class="li-p hidden-xs">
					<%
						DataBase Product = new DataBase();
						User.setTable("product");
						int Productnum = User.getNumber();
						out.print(Productnum);
					%>
				</p>
            </li>
            <li class="data-li">
                <h4>留言数</h4>
					<p class="li-p hidden-xs">
						<%
		                	DataBase Message = new DataBase();
		                	Message.setTable("message");
		                	int msgNum = Message.getNumber();
		                	out.print(msgNum); 
                		%>
					</p>
            </li>
            <li class="data-li">
                <h4>订单数</h4>
					<p class="li-p hidden-xs">
						<%
		                	DataBase Order = new DataBase();
		                	Order.setTable("indent");
		                	ResultSet rs = Order.query("select count(oid) from "+Order.getTable());
		                	int orderNum = 0;
		                	if(rs.next())
		                		 orderNum = rs.getInt(1);
		                	out.print(orderNum); 
                		%>
                		
					</p>
            </li>
        
        </ul>
    </div>
</div>
<div id="main" class="echarts"></div>
<div id="main1" class="echarts"></div>


 </body>
 <script src="js/jquery-1.12.4.min.js"></script>
<script src="js/eCharts.js"></script>

<script type="text/javascript">
    // 基于准备好的dom，初始化echarts实例
    var myChart = echarts.init(document.getElementById('main'));
    var myChart1 = echarts.init(document.getElementById('main1'));
    // 指定图表的配置项和数据
    var option = {
        title: {
            text: '销售图表'
        },
        tooltip: {},
        legend: {
            data: ['销量']
        },
        xAxis: {
            data: ["衬衫", "羊毛衫", "雪纺衫", "裤子", "高跟鞋", "袜子"]
        },
        yAxis: {},
        series: [{
            name: '销量',
            type: 'bar',
            data: [5, 20, 36, 10, 10, 20]
        }]
    };
    myChart1.setOption({
        series: [
            {
                name: '访问来源',
                type: 'pie',
                radius: '55%',
                data: [
                    {value: 235, name: '视频广告'},
                    {value: 274, name: '联盟广告'},
                    {value: 310, name: '邮件营销'},
                    {value: 335, name: '直接访问'},
                    {value: 400, name: '搜索引擎'}
                ]
            }
        ]
    });

    // 使用刚指定的配置项和数据显示图表。
    myChart.setOption(option);
    
    // 获取当前时间
    function time() {
        var h= new Date().getHours();
        var day=new Date().getDay();//获取当前星期0-6
        var all=new Date().toLocaleDateString();
        h = h < 10 ? ("0" + h) : h;
        var m = new Date().getMinutes();
        m = m < 10 ? ("0" + m) : m;
        var s = new Date().getSeconds();
        s = s < 10 ? ("0" + s) : s;
       // document.getElementById("curr_time").innerHTML=y+"年"+(M+1)+"月"+d+"日"+"星期"+day+" "+ h + ":" + m + ":" + s;
        document.getElementById("curr_time").innerHTML=all+" "+ h + ":" + m + ":" + s;
    }
    time();
    setInterval(time,1000);
</script>
</html>
