<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page language="java" import="DataBase.DataBase"%>
<%@ page language="java" import="java.sql.*"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>"> 
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>用户增加</title>
    <link rel="stylesheet" href="css/bootstrap3.3.7.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/wcCss.css">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	
  </head>
  <body>
  	<div class="container">
    <div class="row">
            <div class="head">
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">
                    <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>添加用户
                </button>
                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">添加用户</h4>
                            </div>
                            <div class="modal-body">
                                <form action="userAction!adminAddUser">
                                	<input type="hidden" name="flag" value="1">
                                    <div class="form-group">
                                        <label for="username">用户名</label>
                                        <input type="text" class="form-control" id="username" placeholder="请输入用户名" name="username">
                                    </div>
                                    <div class="form-group">
                                        <label for="userpass">密码</label>
                                        <input type="password" class="form-control" id="userpass" placeholder="请输入密码" name="password">
                                    </div>
                                    <div class="form-group">
                                        <label for="userpassA">确认密码</label>
                                        <input type="password" class="form-control" id="userpassA" placeholder="再次输入密码" name="password1">
                                    </div>
                                    <button type="button" class="btn btn-default btn-right" data-dismiss="modal">关闭
                                    </button>
                                    <button type="submit" class="btn btn-primary" id="addUser">添加</button>
                                    
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <caption class="tHead">用户信息</caption>
                    <thead>
                    <tr>
                        <td>ID</td>
                        <td>用户名</td>
                        <td>姓名</td>
                        <td>邮箱</td>
                        <td>联系电话</td>
                        <td>收货地址</td>
                    </tr>
                    </thead>
                    <%-- 显示用户数据 --%>
                    <%
                    	DataBase database = new DataBase();
                    	database.setTable("user");
                    	ResultSet re = database.query();
                    	if(re!=null){
                    		while(re.next()){
                    			out.print("<tr>");
                    			out.print("<td>"+re.getInt("id")+"</td>");
                    			out.print("<td>"+re.getString("username")+"</td>");
                    			out.print("<td>");
                    			if(re.getString("name")!=null)
                    				out.print(re.getString("name"));
                    			else out.print("-暂未设置-");
                    			out.print("</td>");
                    			out.print("<td>");
                    			if(re.getString("email")!=null)
                    				out.print(re.getString("email"));
                    			else out.print("-暂未设置-");
                    			out.print("</td>");
                    			out.print("<td>");
                    			if(re.getString("phone")!=null)
                    				out.print(re.getString("phone"));
                    			else out.print("-暂未设置-");
                    			out.print("</td>");
                    			out.print("<td>"+"湖南科技学院"+"</td>");
                    			out.print("</tr>");
                    		}
                    	}
                     %>
                </table>
            </div>
            <nav aria-label="Page navigation" style="text-align: center">
                <ul class="pagination">
                    <li>
                        <a href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li>
                        <a href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                </ul>
            </nav>
    </div>
</div>


  </body>
<script src="js/jquery-1.12.4.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
	$(function(){
		$("#addUser").click(function(){
			var username = $("#username").val();
			var password = $("#userpass").val();
			var password1 = $("#userpassA").val();
			if (username == "" || password == "") {                
		        alert("用户名和密码不能为空，请重新输入");                             
		        $("#userpass").val("");
		        $("#userpassA").val("");
		        return false;            
		    }
		    else if(password.length < 6 || password.length > 16){            
		        $("#userpass").val("");
		        $("#userpassA").val("");
				alert("密码必须在6-16位数之内");
				return false;
			}
			else if(password != password1){             
		        $("#userpass").val("");
		        $("#userpassA").val("");
				alert("两次密码不一致");
				return false; 
			}
			else {
				return true;
			}
		});
		/*状态码判断*/
		<%
			String err = (String)session.getAttribute("err");
			if (err != null){
				if(err.equals("suc")){
					out.print("alert('恭喜你添加用户成功！')");
				}
				else if(err.equals("900")){
					out.print("alert('添加失败！该用户已存在')");
				}
				session.removeAttribute("err");//清除err状态吗
			}
		%>
	});
</script>
</html>
