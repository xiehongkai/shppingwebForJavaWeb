<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>"> 
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>衣锦年华后台管理</title>
    <link rel="stylesheet" href="css/bootstrap3.3.7.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="css/tree.css">
    <link rel="stylesheet" href="css/wcCss.css">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<style>

        iframe {
            border: 0;
            width: 100%;
            height: 750px;
            padding: 8px;
        }
        .col-md-10 {
            padding: 0;
        }
        .btn-primary {
        	padding: 10px;
        }
    </style>

  </head>
  <body>
   <div class="navbar navbar-inverse">
    <div class="navbar-header">
        <a href="jsp/admin.jsp" class="navbar-brand">
            <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
            衣锦年华后台
        </a>
    </div>
    <ul class="nav navbar-nav navbar-right">
        <li class="active">
        	<a href="">
        		<span class="glyphicon glyphicon-user" aria-hidden="true"></span>
        		<%-- 显示管理员name --%>
        		<%
        			String userid =(String)session.getAttribute("userid");
					if(userid != null)
					{
						String uname = (String)session.getAttribute("username");
						out.print(uname);
					}
					else {
						out.print("请登录");
					}
        		 %>
        	</a>
        	</li>
        <li class=""><a href=""><span class="glyphicon glyphicon-off" aria-hidden="true"></span></a></li>
    </ul>
    <div class="input-group hidden-sm hidden-xs">
        <input type="text" class="form-control" placeholder="搜索">
        <span class="input-group-btn">
            <button class="btn btn-primary" type="button">
                <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
            </button>
        </span>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-2 col-size">
            <div class="treebox">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <nav class="menu navbar navbar-inverse">
                        <li class="level1"><a><em class="ico ico1"></em>用户管理<i
                                class="down"></i></a>
                            <ul class="level2">
                                <li><a href="jsp/addUser.jsp" target="ifr">用户增加</a></li>
                                <li><a href="jsp/delUser.jsp" target="ifr">用户删除</a></li>
                                <li><a href="jsp/updateUser.jsp" target="ifr">用户更新</a></li>
                            </ul>
                        </li>
                        <li class="level1"><a href="#none"><em class="ico ico1"></em>商品管理<i
                                class="down"></i></a>
                            <ul class="level2">
                                <li><a href="jsp/addProduct.jsp" target="ifr">商品增加</a></li>
                                <li><a href="jsp/delProduct.jsp" target="ifr">商品删除</a></li>
                                <li><a href="jsp/updateProduct.jsp" target="ifr">商品更新</a></li>
                            </ul>
                        </li>
                        <li class="level1"><a href="#none"><em class="ico ico1"></em>留言管理<i
                                class="down"></i></a>
                            <ul class="level2">
                                <li><a href="jsp/Admin/updateMessage.jsp" target="ifr">留言状态</a></li>
                                <li><a href="jsp/Admin/delMessage.jsp" target="ifr">删除留言</a></li>
                            </ul>
                        </li>
                        <li class="level1"><a href="#none"><em class="ico ico1"></em>订单管理<i
                                class="down"></i></a>
                            <ul class="level2">
                                <li><a href="jsp/Admin/orderStatus.jsp" target="ifr">订单状态</a></li>
                                <li><a href="jsp/Admin/delOrder.jsp" target="ifr">删除订单</a></li>
                            </ul>
                        </li>
                    </nav>
                </div>
            </div>
        </div>
        <div class="col-md-10">
            <iframe src="jsp/admin_index.jsp" name="ifr" id="ifr">

            </iframe>
        </div>
    </div>
</div>

<script src="js/jquery-1.12.4.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/easing.js"></script>
<script>
    $(function () {
        $(".treebox .level1>a").click(function () {

            if (!$(this).hasClass("current")) {
                $(this).addClass('current')
                    .find('i').addClass('down') //小箭头向下样式
                    .parent().next().slideDown('slow', 'easeOutQuad') //下一个元素显示
            } else {
                $(this).parent().children('a').removeClass('current') //父元素的兄弟元素的子元素去除"current"样式
                    .find('i').removeClass('down').parent().next().slideUp('slow', 'easeOutQuad'); //隐藏
            }
            if (!$(this).find("em").hasClass("active")) {
                $(this).find(".ico").addClass("active");
            } else {
                $(this).find("em").removeClass("active");
            }
            return false; //阻止事件冒泡
        });
        $(".level2>li").click(function () {
            $("li").removeClass("Lactive");
            $(this).addClass("Lactive");
        });
    })
</script>
 </body>
</html>
