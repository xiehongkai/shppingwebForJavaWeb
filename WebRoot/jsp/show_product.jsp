<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<html>
<head>
	<meta charset='utf-8'>
	<title>我的简易购物网站</title>
	<link href="../css/ad.css" rel='stylesheet' type='text/css'/>
	<style>
	body
	{
		width:100%;
		background:#f0f0f0;
	}
	#top
	{
		width:100%;
		height:120px;
		background:transparent url(../img//log.jpg) no-repeat scroll center center;
		overflow:hidden;
		z-index:1;
	}
	#sort
	{
		position:relative;
		width:100%;
		height:50px;
		background:#f0f0f0;
		padding:0 12%;
		margin-bottom:8;
	}
	#ul li
	{	
		color:white;
		margin:0 1px;
		float :left;
		list-style-type:none;
		height:100%;
		background:rgba(194,0,0,0.3);
		width:180px;
		text-align:center;
	}
	#ul li a
	{
		color:white;
		margin:0 1px;
		text-decoration:none;
	}
	#body
	{	
		overflow:visible;
		width:100%;
		height:850px;
		z-index:1;
		position:relative;
		background:rgba(147,281,226,0.2);
	}
	#body #left
	{
		width:15%;
		height:850px;
		position:relative;
		float:left;
		background:
		url(../img//bgleft.jpg) no-repeat scroll center center;
	}
	#left #div1
	{
		width:100%;
		height:20%;
		background:rgba(31,31,31,0.9);
	}
	#left #div2
	{
		width:100%;
		height:79.9%;
		background:rgba(166,166,166,0.6);
		position:absolute;
		top:20.1%;
	}
	#body #bgimg
	{
		width:70%;
		height:850px;
		z-index:1;
		background:
		url(../img//bg2.png) no-repeat scroll center center;
		position:relative;
		float:left;
	}
	#bgimg #divblack
	{
		z-index:2;
		width:97.5%;
		height:850px;
		background:rgba(21,21,21,0.7);
		display:none;
		
	}
	#bgimg #sort1
	{
		
		z-index:3;
		width:826px;
		height:384px;
		position:absolute;
		top:10px;
		left:40px;
		display:none;
	}
	#bgimg #sort2
	{
		background:url(../img//sort2.png) no-repeat scroll center center;
		z-index:3;
		width:826px;
		height:448px;
		position:absolute;
		top:400px;
		left:40px;
		display:none;
	}
	#body #right
	{
		width:15%;
		height:100%;
		position:relative;
		float:right;
	}
	#bottom
	{
		width:100%;
		height:115px;
		margin:10 auto;
		position:relative;
		bottom:0px;
		background:url('../img/bottom.jpg');
	}
	input[type='submit']
	{
		background:pink;
		color:black;
		margin:20px 25px;
		border-radius:50px;
	}
	#right #tx
	{
		width:100px;
		margin:10px auto;
		height:100px;
		border:1px solid blue;
		border-radius:50px;
		background: url(../img/tx.png);
	}
	#right #right_2
	{
		margin:5 auto;
		padding:5 0;
	}
}
</style>
<script src="../js/1.js"></script>
</head>
<body>
	<div id='top'></div>
	<div id='sort'>
		<ul id='ul'>
			<a href='index.jsp'><li><p>网站首页</p></li></a>
			<a href=''><li><p>产品展示</p></li></a>
			<a href='liuyan.jsp'><li><p>顾客留言</p></li></a>
			<a href='we.jsp'><li><p>关于我们</p></li></a>
			<a href='lianxi.jsp'><li><p>联系我们</p></li></a>
		</ul>
	</div>
	<div id='body'>
		<div id='left'>
			<div id='div1'></div>
			<div id='div2'></div>
		</div>
		<div id='bgimg'>
			<div id='divblack'></div>
			<div id='sort1'>
				<img src='../img//sort1.png' usemap='#map_sort1'>
				<map name='map_sort1'>
				<!-- 热销爆款-->
					<area shape='rect' coords="0,0,414,384" href="productAction?flag=1">
					<!-- 2018夏季新款-->
					<area shape='rect' coords="439,0,826,384" href="productAction?flag=2">
				</map>
			</div>
			<div id='sort2'>
	
				<img src='../img//sort2.png' usemap='#map_sort2'>
					<map name='map_sort2'>
						<area shape='rect' coords="0,0,256,212" href="productAction?flag=3">
						<area shape='rect' coords="282,0,541,212" href="productAction?flag=4">
						<area shape='rect' coords="570,0,830,212" href="productAction?flag=5">
						<area shape='rect' coords="0,235,256,450" href="productAction?flag=6">
						<area shape='rect' coords="280,235,541,450" href="productAction?flag=7">
						<area shape='rect' coords="570,235,830,450" href="productAction?flag=8">
					</map>
			</div>
		</div>
		<div id='right'>
			<%
				String userid =(String)session.getAttribute("userid");
				if(userid!=null)
				{
					String uname = (String)session.getAttribute("username");
					out.println("<div id='tx'></div>");
					out.println("<center>Welcome <a href='userAction!userinfo'>"+uname+"</a><br/>");
					out.println("<a href='userAction!logout'>登出</a></center>");
				}
				else//如果没有登录就出现登录注册按钮
				{
					out.println("<div id='tx'></div>");
					out.println("<div id='right_2'>");
					out.println("<table>");
					out.println("<tr>");
					out.println("<table>");
					out.println("<td><input type='submit' name='log_sub' formaction='handle.jsp' value='登录'></td>");
					out.println("<td><input type='submit' name='reg_sub' formaction='handle.jsp' value='注册'></td>");
					out.println("</tr></table>");
					out.println("</div>");
				}
			%>
		</div>
		</div>
		
	</div>
	<!--下面这个大盒子是放新品广告区域  -->
	<div id="ad">
		<a target="_bank" href="new.jsp?date=20190604"><div id="new0604"></div></a>
			<div id='new0604info'>
				<a target="_bank" href="../jsp/info.jsp?pro=new&id=99"><div class="first"></div></a>
				<ul>
					<a target="_bank" href="../jsp/info.jsp?pro=new&id=100" class='clsa'><li><img src="../img/new/2019062402.png"></li></a>
					<a target="_bank" href="../jsp/info.jsp?pro=new&id=101" class='clsa'><li><img src="../img/new/2019062403.png"></li></a>
					<a target="_bank" href="../jsp/info.jsp?pro=new&id=102" class='clsa'><li><img src="../img/new/2019062404.png"></li></a>
					<a target="_bank" href="../jsp/info.jsp?pro=new&id=103" class='clsa'><li><img src="../img/new/2019062405.png"></li></a>
					<a target="_bank" href="../jsp/info.jsp?pro=new&id=104" class='clsa'><li><img src="../img/new/2019062406.png"></li></a>
					<a target="_bank" href="../jsp/info.jsp?pro=new&id=105" class='clsa'><li><img src="../img/new/2019062407.png"></li></a>
				</ul>
			</div>
		<%--20190624 区域结束 --%>
		<%--20190528 区域 --%>
		<%--5.28新品图 --%>
		<a target="_bank" href="new.jsp?date=20190528"><div id="new0528"></div></a>
			<div id='new0528info'>
				<%--第一张大图 --%>
				<a target="_bank" href="../jsp/info.jsp?pro=new&id=106"><div class="first first528"></div></a>
				<ul id='ul528'>
					<a target="_bank" href="../jsp/info.jsp?pro=new&id=107" class='clsa'><li><img src="../img/new/2019052802.png"></li></a>
					<a target="_bank" href="../jsp/info.jsp?pro=new&id=108" class='clsa'><li><img src="../img/new/2019052803.png"></li></a>
					<a target="_bank" href="../jsp/info.jsp?pro=new&id=109" class='clsa'><li><img src="../img/new/2019052804.png"></li></a>
					<a target="_bank" href="../jsp/info.jsp?pro=new&id=110" class='clsa'><li><img src="../img/new/2019052805.png"></li></a>
					<a target="_bank" href="../jsp/info.jsp?pro=new&id=111" class='clsa'><li><img src="../img/new/2019052806.png"></li></a>
					<a target="_bank" href="../jsp/info.jsp?pro=new&id=112" class='clsa'><li><img src="../img/new/2019052807.png"></li></a>
				</ul>
			</div>
		<%--20190528 区域结束 --%>
		<!-- <a href="new.jsp?date=20190520"><div id="new0520"></div></a>
		<div id=new0520info></div>
		<a href="new.jsp?date=20190513"><div id="new0513"></div></a>
		<div id=new0513info></div> -->
		
	</div>
	<div id='bottom'>
	</div>
<script type="text/javascript">
	 //鼠标移动判定开始分类的移动特效
	$current = $("#ul li:eq(1)"); //匹配第二个列表元素
	$current.css("background","rgba(194,0,0,1)");
	$("#ul li").click(function(){
		$("#ul li").css("background","rgba(194,0,0,0.3)");//淡色所有元素
		$(this).css("background","rgba(194,0,0,1)");//加深当前元素
		$current = $(this);//记录当前元素
	});
	$("#ul li").hover(function(){
	$(this).css("background","rgba(194,0,0,1)");
	},function(){
	$(this).css("background","rgba(194,0,0,0.3)");
	$current.css("background","rgba(194,0,0,1)");//当前元素始终加深
	});
	//鼠标移动判定结束
	
	$("#bgimg div").slideToggle(1000);//分类图缓慢出现
</script>
</body>
</html>