<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page language="java" import="DataBase.DataBase"%>
<%@ page language="java" import="java.sql.*"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">

<title>商品删除</title>
<link rel="stylesheet" href="css/bootstrap3.3.7.css">
<link rel="stylesheet" href="css/bootstrap-theme.min.css">
<link rel="stylesheet" href="css/wcCss.css">
</head>

<body>
	<div class="container">
	<div class="row">
        <div class="head">
            <h4>商品删除：</h4>
        </div>
        
        <%
			// 获取数据库中的数据
			DataBase database = new DataBase();
			database.setTable("product");
			ResultSet re = database.query();
		%>
        <!-- 显示商品列表 -->
        <div class="table-responsive">
            <table class="table table-hover table-bordered">
                <caption class="tHead">商品信息</caption>
                <thead>
                <tr>
                    <td>ID</td>
                    <td>商品名称</td>
                    <td>数量</td>
                    <td>价格</td>
                    <td>URL</td>
                    <td>操作</td>
                </tr>
                </thead>
                <% 
                	while(re.next()){
							out.print("<tr>");
							out.print("<td>"+re.getInt("id")+"</td>");
							out.print("<td class='omit'>"+re.getString("name")+"</td>");
							out.print("<td>"+re.getInt("scount")+"</td>");
							out.print("<td>"+re.getDouble("price")+"</td>");
							out.print("<td>"+re.getString("url")+"</td>");
							out.print("<td><a type='button' class='btn btn-danger btn-sm'"
							+"href='productAction!adminDelProduct?delId=" + re.getInt("id")
                    		+"'>");
                    		out.print("<span class='glyphicon glyphicon-trash' aria-hidden='true'></span>删除</a></td>");
							out.print("</tr>");
						}
                %> 
            </table>
        </div>
        <nav aria-label="Page navigation" style="text-align: center">
            <ul class="pagination">
                <li>
                    <a href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li>
                    <a href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</div>
  </body>
</html>
