<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="DataBase.DataBase" %>
<%@ page import="java.sql.ResultSet"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
	<title>我的简易购物网站</title>
	<meta charset='utf-8'>
<style>
	body
	{
		height:2000px;
		width:100%;
		background:#f0f0f0;
	}
	#top
	{
		width:100%;
		height:120px;
		background:transparent url(img//log.jpg) no-repeat scroll center center;
		overflow:hidden;
		position:fixed;
		top:0px;
		z-index:6;
	}
	#sort
	{
		position:fixed;
		width:100%;
		height:50px;
		background:#f0f0f0;
		padding:0 12%;
		margin-bottom:8;
		z-index:99;
		top:120px;
	}
	#ul li
	{	
		color:white;
		margin:0 1px;
		float :left;
		list-style-type:none;
		height:100%;
		background:rgba(194,0,0,0.3);
		width:180px;
		text-align:center;
	}
	#ul li a
	{
		color:white;
		margin:0 1px;
		text-decoration:none;
	}
	#center
	{	
		overflow:hidden;
		width:100%;
		height:545px;
		position:absolute;
		top:190px;
		z-index:2;
	}
	#center #center_left
	{
		width:500px;
		height:100%;
		background:rgba(255,255,255,0.5);
		position:absolute;
		top:0px;
		left:0px;
	}
	#center #center_left #pic
	{
		width:400px;
		height:400px;
		border:1px solid #f0f0f0;
		position:absolute;
		top:0px;
		left:80px;
	}
	#pic img
	{
		width:400px;
		height:400px;
	}
	#center #center_center
	{
		width:100%;
		height:100%;
		background:rgba(255,255,255,0.5);
		position:absolute;
		top:0px;
		left:500px;
	}
	#center #center_right
	{
		width:270px;
		height:100%;
		background:rgba(255,255,255,0.5);
		position:absolute;
		top:0px;
		left:1030px;
	}
	#bgimg
	{
		width:100%;
		height:500px;
		z-index:1;
		position:fixed;
		top:190px;
		background:
		url(img//bg2.png) no-repeat scroll center center;
	}
	#bgimg #divblack
	{
		z-index:2;
		width:100%;
		height:850px;
		background:rgba(21,21,21,0.7);
		display:none;
		
	}
	#center #right
	{
		width:15%;
		height:100%;
		position:relative;
		float:right;
	}
	#bottom
	{
		margin:10 auto;
		width:100%;
		height:600px;
		background:#f0f0f0;
		position:absolute;
		top:1030;
	}
	input[type='submit']
	{
		background:pink;
		color:black;
		margin:20px 25px;
		border-radius:50px;
	}
	#right #tx
	{
		width:100px;
		margin:10px auto;
		height:100px;
		border:1px solid blue;
		border-radius:50px;
		background: url(img/tx.png);
	}
	#right #right_2
	{
		margin:5 auto;
		padding:5 0;
	}
	#center #center-left ul
	{
		margin:0;
		padding:0;
		list-style:none;
	}
	#center  #list
	{
		display:flex;
		list-style:none;
		position:absolute;
		top:390px;
		left:50px;
	}
	#center  #list img
	{
	   width:60px;
	   height:60px;
	   margin:5px 10px;
	   display:block;
	}
	#center #detail
	{
		display:none;
		width:400px;
		height:400px;
		border:0px;
		position:absolute;
		top:0px;
		left:480px;
		z-index:99;
		background-size:280%;
	}
	#center #pic #cover
	{
		width:150px;
		height:150px;
		position:absolute;
		top:0px;
		left:0px; 
		background:rgba(0,160,228,0.3);
		z-index:3;
		display:none;
	}
	#center_center #center2
	{
		width:100%;
		height:25px;
		position:absolute;
		top:50px;
		left:20px;
		color:black;
		font-weight:bold;
	}
	#center_center #center2 #top
	{
		width:100%;
		height:25px;
		background:rgba(255,255,255,0);
		position:absolute;
		top:0px;
		color:red;
	}
	#center_center #center2 #cr
	{
		width:100%;
		height:50px;
		position:absolute;
		top:40px;
		color:rgb(255,0,54);
		font-weight:bold;
	}
	#center_center #center2 #bottom2
	{
		width:100%;
		height:50px;
		position:absolute;
		top:110px;
		margin-top:0px;
	}
	#center_center #bt
	{
		width:100%;
		height:50px;
		position:absolute;
		top:200px;
		left:20px;
	}
	#bt p
	{
		display:inline;
		margin:10px 0px;
	}
	#bt table
	{
		position:absolute;
		top:0px;
		left:-5px;
	}
	#bt tr
	{
		margin:0px 0px;
		color:black;
		font-size:18;
		font-weight:bold;
	}
	#bt tr img
	{
		margin:20px 0px;
	}
	input['type=number']
	{
		margin-top:20px;
	}
	#bt #size input[type='button']
	{
		width:39px;
		height:39px;
		border:0.5px solid #f1f1f1; 
		background:white;
	}
	#bt #size
	{
		position:absolute:
		top:100px;
		left:80;
	}
</style>
<script src="./js/1.js"></script>
</head>
<body>
	<div id='top'></div>
	<div id='sort'>
		<ul id='ul'>
			<a href='./jsp/index.jsp'><li><p>网站首页</p></li></a>
			<a href='./jsp/show_product.jsp'><li><p>产品展示</p></li></a>
			<a href='./liuyan.jsp'><li><p>顾客留言</p></li></a>
			<a href='./we.jsp'><li><p>关于我们</p></li></a>
			<a href='./lianxi.jsp'><li><p>联系我们</p></li></a>
		</ul>
	</div>
	<div id='bgimg'>
			<!-- 中间区域透明黑色背景-->
			<div id='divblack'></div>
	</div>
	<%
		String src;
		String pid = request.getParameter("id");
		if(pid!=null)
		{
			session.setAttribute("pid", pid);//保存商品id到会话中,以便购买的时候处理
			src="./img/pro/pro2/"+pid;
		}
		else //否则没有商品id，显示测试模板
		   src="img/pro/pro2/1";
	 %>
	<div id='center'>
		<div id='center_left'>
			<div id='pic'>
				<img src='<%= src+"/1.jpg"%>' >
				<div id='cover'></div>
			</div>
			<ul id='list'>
				<li><img src='<%= src+"/1.jpg"%>' title='1'>
				<li><img src='<%= src+"/2.jpg"%>' title='2'>
				<li><img src='<%= src+"/3.jpg"%>' title='3'>
				<li><img src='<%= src+"/4.jpg"%>' title='4'>
				<li><img src='<%= src+"/5.jpg"%>' title='5'>
			</ul>
			<div id='detail'></div>
		</div>
		<div id='center_center'>
			<!-- 中间区域的详情 -->
			<%
				DataBase database=new DataBase();
				ResultSet rs=database.query("select * from product where id='"+pid+"'");
				if(rs!=null)
				{
					while(rs.next())
					{
						out.println("<p style='font-size:16px;font-weight:bold; margin:15px 20px;'>"+rs.getString(2)+"</p></br>");
						out.println("<div id='center2'>");
						out.println("<div id='center2'>");
						out.println("<div id='top'>夏季热卖 时尚大促销</div>");
						out.println("<div id='cr'>价格:<span style='font-size:17' font-weight:bold;>￥</span><h1 style='display:inline;'>"+rs.getInt(4)+"</h1></div>"); 
						out.println("<div id='bottom2'>月销量:<span style='color:rgb(255,0,54);font-weight:bold;'>"+rs.getString(3)+"</p></div>");
						out.println("</div>");
						out.println("<div id='bt'>");
						//提交按钮上有单独的处理action
						out.println("<form 	'info_frm' action='' method='post'>");
						out.println("<table>");
						out.println("<tr>");
							out.println("<td>尺码:</td>");
							out.println("<td>");
								out.println("<div id='size'>");
									out.println("<input type='button' value='S'>");
									out.println("<input type='button' value='M'>");
									out.println("<input type='button' value='L'>");
									out.println("<input type='button' value='XL'>");
								out.println("</div>");
							out.println("</td>");
						out.println("</tr>");
						out.println("<tr>");
							out.println("<td>颜色分类:</td>");
							out.println("<td>");
								out.println("<div id='color'>");
									out.println("<img src="+src+"/color1.jpg title='1' width='40' height='40'>");
									out.println("<img src="+src+"/color2.jpg title='2' width='40' height='40'>");
									//隐藏域保存颜色
									out.println("<input type='hidden' name='color' value='0'>");
								out.println("</div>");
							out.println("</td>");
						out.println("</tr>");
						out.println("<tr>");
							out.println("<td>数量:</td>");
							out.println("<td><input type='number' name='number' value='1'> 件</td>");
						out.println("</tr>");
						out.println("<tr>");	
							out.println("<td><input style='border:2px solid red;border-radius:0px; width:180px; height:40px;' type='submit' name='buy' value='立即购买' formaction='indentAction!buy'></td>");
							out.println("<td><input style='background:rgb(255,0,54);border-radius:0px; width:180px; height:40px;' type='submit' name='addcar' value='加入购物车' formaction='carAction!addcar'></td>");
						out.println("</tr>");	
						out.println("<input type='hidden' name='size' value='S'>");
						/* 隐藏域保存商品名称 */
						out.println("<input type='hidden' name='pname' value='"+rs.getString(2)+"'>");
						/* 隐藏域保存商品单价 */
						out.println("<input type='hidden' name='uprice' value='"+rs.getString(4)+"'>");
						out.println("<input type='hidden' name='pid' value='"+rs.getInt(1)+"'>");
						out.println("</table>");
						out.println("</div>");
					}//end while rs.next()
				}//end if rs!=null
			 %>
		</div>		
		<!-- 表单元素的选择判定 -->
		<script type='text/javascript'>
			
			$img = $("<img src='img/border.png'>");//创建图片边框;
			$img2 = $("<img src='img/border.png'>");
			//尺码中的特效
			$("#size")
			$("#size :button").on('click',function(){
				var mytop = $(this).position().top-20;
				var myleft =  $(this).position().left;
					$img.css({
				"z-index":"1",
				"width":"40px",
				"height":"40px",
				"position":"absolute",
				"top":mytop+"px",
				"left":myleft+"px"
			});//防止被盖住
				$(this).before($img);
				//$(this).css("border","3px solid red");
				$value = $(this).attr('value');
				//name=size的隐藏域的value值用于保存选择的尺寸大小
				$("[name='size']").attr('value',$value);
			});
			
			//颜色上的特效
			$("#color img").on('click',function(){
				//$(this).css("border","2px solid red");
				var mytop = $(this).position().top;
				var myleft =  $(this).position().left;
					$img2.css({
				"z-index":"1",
				"width":"40px",
				"height":"40px",
				"position":"absolute",
				"top":mytop+"px",
				"left":myleft+"px"
			});//防止被盖住
				$(this).before($img2);
				//alert(mytop+'   '+myleft);
				$value = $(this).attr('title');
				//name=color的隐藏域的value值用于保存选择的颜色
				$("[name='color']").attr('value',$value);
				
				//这一块是单击颜色快在主图区显示的代码
				$flag_src = $(this).attr('src');
				$("#pic img").attr('src',$flag_src);//然后大图src等于这张图src
				$("#detail").css("background",'url('+$flag_src+')');//detail也要改变
				//到这结束
			});
		</script>
		<div id='center_right'>
			<!-- <div id='tx'>
			</div>
			
			<div id="right_2">
				<?php
					if(isset($_GET['type']))
						echo "hahaha";
				?>
				<form name='frm'>
				<tr>
				<td><input type="submit" name='log_sub' formaction='handle.php' value='登录'></td>
				<td><input type="submit" name='reg_sub' formaction='handle.php' value='注册'></td>
				</tr>
				<form>
			</div> -->
		</div>
	</div>
	<div id='bottom'>
		<!-- <center><p><embed src='mp4//衣锦年华女装店宣传视频.mp4' width='800' 
		height='100%' ></embed></p></center>-->
		
	</div>
<script type="text/javascript">
	 //鼠标移动判定开始
	$current = $("#ul>li:eq(1)"); //匹配第二个列表元素
	$current.css("background","rgba(194,0,0,1)");
	$("#ul li").click(function(){
		$("#ul li").css("background","rgba(194,0,0,0.3)");//淡色所有元素
		$(this).css("background","rgba(194,0,0,1)");//加深当前元素
		$current = $(this);//记录当前元素
	});
	$("#ul li").hover(function(){
	$(this).css("background","rgba(194,0,0,1)");
	},function(){
	$(this).css("background","rgba(194,0,0,0.3)");
	$current.css("background","rgba(194,0,0,1)");//当前元素始终加深
	});
	//鼠标移动判定结束
	
	$("#bgimg div").slideToggle(1000);
</script>
<script type='text/javascript'>
	//方框随鼠标移动//js写的蓝色方块鼠标移入事件
	var pic =document.querySelector('#pic');
	var cover = document.querySelector('#cover');
	pic.addEventListener('mousemove',(function(e){
		var x = e.clientX,
		    y = e.clientY,
		    cx = pic.getBoundingClientRect().left,
	        cy =pic.getBoundingClientRect().top;
		var tx=x-cx-75,ty=y-cy-75;
		if(tx<0)tx = 0;
		if(tx>400-150)tx=400-150;
		if(ty<0)ty =0;
		if(ty>400-150)ty=400-150;
		detail.style.display='block';
		detail.style.backgroundPosition=tx/250*100+'%'+ty/250*100+'%';
		$("#cover").show();
		$("#cover").css({
		"left":tx,
		"top":ty,
		});
	}));
	//鼠标移出方块消失；
	$("#pic").mouseout(function(){
		$("#cover").hide();
		$("#detail").hide();
	});
	
	//图片切换 
	$flag_img = $("#list img:first")
	$flag_img.css("border","3px solid black");
	$("#list img").hover(function(e){
		$flag_img.css("border","none");
		$(e.target).css("border","3px solid black");
		$flag_img = $(e.target);//记录当前元素
		$flag_src = $flag_img.attr('src');//记录当前图片的src
		$("#pic img").attr('src',$flag_src);//然后大图src等于这张图src
		$("#detail").css("background",'url('+$flag_src+')');//detail也要改变
	});
		//手动执行代码,解决第一次移入鼠标不显示的问题bug
		$flag_img = $('#list img:first');//记录当前元素
		$flag_src = $flag_img.attr('src');//记录当前图片的src
		$("#pic img").attr('src',$flag_src);//然后大图src等于这张图src
		$("#detail").css("background",'url('+$flag_src+')');//detail也要改变	
		//完毕
</script> 
</body>
</html>
