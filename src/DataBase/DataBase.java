package DataBase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Statement;

public class DataBase 
{
	String dirverClass;//连接类
	String url;//连接的url
	String user;//连接的用户名
	String password;//数据库密码
	String table;//使用的数据表
	Connection conn;//连接句柄
	String sql;//用于调试的sql语句
	Statement stmt;//执行sql的声明类
	ResultSet rs;//返回的结果集
	public DataBase() 
	{
		// TODO Auto-generated constructor stub
		dirverClass = "com.mysql.jdbc.Driver";
		url = "jdbc:mysql://localhost:3306/shopping";
		user= "root";
		password="";
		table="user";//默认的数据表
		sql="";
		try 
		{
			Class.forName(dirverClass).newInstance();
			conn=DriverManager.getConnection(url,user,password);
			stmt=(Statement) conn.createStatement();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	public String getTable() {
		return table;
	}
	public void setTable(String table) {
		this.table = table;
	}
	public ResultSet query()//返回查询的数据集合
	{
		sql="select * from "+table;
		printSql();
		try {
			rs = stmt.executeQuery(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("sql执行查询语句异常，检查后重试");
		}
		return rs;
		
	}
	public ResultSet query(String sql)//返回查询的数据集合,重载方法
	{
		this.sql = sql;
		printSql();
		try {
			rs = stmt.executeQuery(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("sql执行查询语句异常，检查后重试");
		}
		return rs;
	}
	public ResultSet queryFromOne(String userid)//返回查询的数据集合
	{
		sql="select * from " + table + " where id = " + userid;
		printSql();
		try {
			rs = stmt.executeQuery(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("sql执行查询语句异常，检查后重试");
		}	
		return rs;
	}
public int getNumber(){
		
		sql = "select count(name) from " + table;
		int num=0;
		try {
			rs = stmt.executeQuery(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("sql执行查询语句异常，检查后重试");
		}
		if(rs!=null)
		{
			try {
				rs.next();
				num = rs.getInt(1);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		return num;
	}
	//恢复正确的id自增值
	public void restoreIdAutoIncrement()
	{
		sql="select id from "+table+" order by id desc limit 1";
		int upid=0;
		try {
			rs = stmt.executeQuery(sql);
			rs.next();
			upid = rs.getInt(1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}//end try catch
		printSql();
		System.out.println("before modify upid is "+upid);
		sql="alter table "+table+" auto_increment="+(upid+1);
		try {
			printSql();
			stmt.execute(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("next insert data's id is "+(upid+1));
	}
	
	public ResultSet queryFromOneoId(String userid)//返回查询的数据集合
	{
		sql="select * from " + table + " where oid = " + userid;
		printSql();
		try {
			rs = stmt.executeQuery(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("sql执行查询语句异常，检查后重试");
		}	
		return rs;
	}
	
	public void printSql()
	{
		System.out.println(sql);
	}
}
