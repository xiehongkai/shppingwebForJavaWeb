package Dao;

public interface IUserDao 
{
	public String userinfo();//查看用户个人信息	
	public String login();//用户登录
	public String regist();//用户注册
	public String logout();//用户登出
	// 管理员添加用户
	public String adminAddUser();
	public String adminDelUser();
	public String update();
	public String updateInfo();
	public String userUpdateInfo();
	
}
