package Po;

import java.util.Date;

public class Message {
	
	private int id;			// id
	private String name;	// 姓名
	private String email;	// 邮箱
	private String address;	// 地址
	private String msg;	// 留言
	boolean status;		// 是否（有不正常的留言）允许显示在前台页面上
	private Date date;
	// 默认构造方法		留言成功之后需通过管理员的允许才能显示在页面上
	public Message(){
		date = new Date();
		status = false;
	}
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
}
