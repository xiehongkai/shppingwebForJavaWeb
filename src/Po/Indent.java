package Po;

import java.util.Date;


public class Indent {
	private String oid;//订单号（日期+时间+随机生成的7位数，共15位）
	private User user;//外键用户，参照用户id号
	private Product product;//外键商品，对应商品id号
	private int paymentNumber;//支付宝付款号后8位
	private String pname;//商品名称
	private String size;//尺寸
	private String color;//颜色
	private int number;//购买数量
	private double uprice;//购买单价
	private double tprice;//总价
	private String rname;//收件人姓名
	private String phonenumber;//电话号码
	private String address;//收货地址
	private String send;//发货状态，默认未发货
	private String confirm;//用户确认收货状态，默认未收货
	private String status;//订单完成状态，默认未完成
	private Date cdate;//订单创建时间，根据创建时间生成
	private Date fdate;//订单完成时间，根据完成状态更新或用户确认收货更新
	//默认的构造方法
	public Indent() {
		// TODO Auto-generated constructor stub
		cdate=new Date();//创建时间从示列化的时候开始
		send = "未发货";
		confirm = "未收货";
		status="未完成";
	}
	public String getOid() {
		return oid;
	}
	public void setOid(String oid) {
		this.oid = oid;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public int getPaymentNumber() {
		return paymentNumber;
	}
	public void setPaymentNumber(int paymentNumber) {
		this.paymentNumber = paymentNumber;
	}
	public String getPname() {
		return pname;
	}
	public void setPname(String pname) {
		this.pname = pname;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public double getUprice() {
		return uprice;
	}
	public void setUprice(double uprice) {
		this.uprice = uprice;
	}
	public double getTprice() {
		return tprice;
	}
	public void setTprice(double tprice) {
		this.tprice = tprice;
	}
	public String getSend() {
		return send;
	}
	public void setSend(String send) {
		this.send = send;
	}
	public String getConfirm() {
		return confirm;
	}
	public void setConfirm(String confirm) {
		this.confirm = confirm;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCdate() {
		return cdate;
	}
	public void setCdate(Date cdate) {
		this.cdate = cdate;
	}
	public Date getFdate() {
		return fdate;
	}
	public void setFdate(Date fdate) {
		this.fdate = fdate;
	}
	public String getRname() {
		return rname;
	}
	public void setRname(String rname) {
		this.rname = rname;
	}
	public String getPhonenumber() {
		return phonenumber;
	}
	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
}
