package Po;

public class CarProduct
{
	User user;//用户id
	Product product;//商品id;
	int cid;//购物车id
	String pname;//商品名称;
	int number;//选择的商品数量
	String size;//选择的商品尺寸
	String color;//商品颜色
	double uprice;//商品单价
	
	public CarProduct()
	{
		size="S";//默认s吗;
		color="1";//默认颜色1
		number=1;//商品默认数量为1
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getCid() {
		return cid;
	}

	public void setCid(int cid) {
		this.cid = cid;
	}

	public String getPname() {
		return pname;
	}

	public void setPname(String pname) {
		this.pname = pname;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public double getUprice() {
		return uprice;
	}

	public void setUprice(double uprice) {
		this.uprice = uprice;
	}
	
}
