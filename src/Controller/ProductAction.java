package Controller;

import java.util.Map;
import SessionFactory.HibernateSessionFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ModelDriven;
import Po.Product;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ModelDriven;

public class ProductAction implements ModelDriven<Product>{
	private Product product = new Product();
	private String flag;//自动注入参数
	String delId;
	// 更新id
	String updateId;
	
	Session session = HibernateSessionFactory.getSession();
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String execute()
	{
		System.out.println("product action success deal with");
		Map request = (Map) ActionContext.getContext().get("request");
		System.out.println("falg is:"+flag);
		
		switch(Integer.parseInt(flag))
		{
			case 1:System.out.println("显示hot信息"); 
			case 2:System.out.println("显示new信息"); 
			case 3:System.out.println("显示dress信息"); 
			case 4:System.out.println("显示tshirt信息"); 
			case 5:System.out.println("显示leisi信息"); 
			case 6:System.out.println("显示double_tao信息"); 
			case 7:System.out.println("显示halfdress信息"); 
			case 8:System.out.println("显示trousers信息"); 
		}
		return "success";
	}
	public String proInfo()
	{
		System.out.println("进入info信息页面");
		return "productinfo";
		
	}
	public Product getModel() {
		// TODO Auto-generated method stub
		return this.product;
	}
	public String getUpdateId() {
		return updateId;
	}

	public void setUpdateId(String updateId) {
		this.updateId = updateId;
	}

	// 管理源增加商品
	public String adminAddProduct(){
		
		Transaction transaction = session.beginTransaction();
		//获取会话，保存各种状态
		Map httpsession = ActionContext.getContext().getSession();
		// 获取从表单上提交的数据
		String name = product.getName();
		int scount = product.getScount();
		double price = product.getPrice();
		String url = product.getUrl();
		
		// 检查商品是否重名
		String hql="from User where username='"+name+"'";
		Query query = session.createQuery(hql);	
		if(query.list().isEmpty())
		{
			System.out.println("数据库不存在同名商品");
			System.out.println("开始插入商品");
			session.save(product);
			httpsession.put("err", "suc");
		}
		else
		{
			System.out.println("数据库存在同名商品");
			httpsession.put("err", "900");
		}
		
		transaction.commit();
		session.close();
		return "addProduct";
	}
	
	// 管理员删除商品
	public String adminDelProduct(){
		
		System.out.println("delId is:" + delId);
		Transaction transaction = session.beginTransaction();
		Product temp = (Product)session.load(Product.class, new Integer(delId));
		session.delete(temp);
		// 提交事务
		transaction.commit();
		session.close();
		return "delProduct";
	}
	
	
	// 保存需要更新的id
	public String update(){
		//获取会话
		Map mysession = ActionContext.getContext().getSession();
		System.out.print("delId is:"+delId);
		mysession.put("proid",""+delId);//会话中保存用户id
		return "updateinfo";
	}
	
	// 管理员更新商品信息
	public String updateInfo(){
		System.out.println("update id:" + updateId);
		Transaction transaction = session.beginTransaction();
		Map httpsession = ActionContext.getContext().getSession();
		
		// 获取表单提交的数据
		String name = product.getName();
		int scount = product.getScount();
		double price = product.getPrice();
		String url = product.getUrl();
		// 输出显示表单上的数据是否获取到了
		System.out.println("name is:"+name+"scount is:"+scount+"price is:"+price+"url is:"+url);
		try{
			Product temp = (Product)session.load(Product.class, new Integer(updateId));
			temp.setName(name);
			temp.setPrice(price);
			temp.setScount(scount);
			temp.setUrl(url);
			httpsession.put("err", "proUpdateSu");
		}catch(Exception e){
			e.printStackTrace();
			// 设置失败状态码
			httpsession.put("err", "ProUpdateEr");
		}
		
		transaction.commit();
		session.close();
		return "update";
	}
	
	
	public String getDelId() {
		return delId;
	}

	public void setDelId(String delId) {
		this.delId = delId;
	}
}
