package Controller;


import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import Po.User;
import SessionFactory.HibernateSessionFactory;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ModelDriven;

public class UserAction implements ModelDriven{
	private User user = new User();
	Session session = HibernateSessionFactory.getSession();
	//获取httpsession对象
	Map httpSession = ActionContext.getContext().getSession();
	String flag;
	String delId;
	String updateId;
	String uinfo_sub;//用户在个人信息页面单机了提交按钮
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getDelId() {
		return delId;
	}
	public void setDelId(String delId) {
		this.delId = delId;
	}
	public String getUpdateId() {
		return updateId;
	}
	public void setUpdateId(String updateId) {
		this.updateId = updateId;
	}
	public String execute()
	{
		//System.out.println("username is:"+user.getUsername());
		//System.out.println("password is:"+user.getPassword());
		return "success";
	}
	public String userinfo()//用户的个人信息
	{
		String userid = (String) httpSession.get("userid");
		//如果用户超时就是停留一个页面很久session中保存的userid过期了
		if(userid==null)
		{
			httpSession.put("err","1");
			return "success";
		}
		System.out.println("userid is:"+userid);
		System.out.println("查看用户id"+userid+"的个人信息页面");
		//查询用户的信息
		User user1 = (User) session.get(User.class,new Integer(userid));
		//保存user对象信息到会话中
		httpSession.put("user",user1);
		return "userspace";
	}
	public String login()
	{
		//获取会话
		Map mysession = ActionContext.getContext().getSession();
		Transaction transaction = session.beginTransaction();
		String hql ="from User";
		Query query = session.createQuery(hql);
		System.out.println("username is:"+user.getUsername());
		System.out.println("password is:"+user.getPassword());
		List list =query.list();
		for(int i = 0;i<list.size();i++)
		{
			User t = (User) list.get(i);
			//如果登录用户名和密码匹配了
			if(t.getUsername().equals(user.getUsername())&&t.getPassword().equals(user.getPassword()))
			{
				mysession.put("userid",""+t.getId());//会话中保存用户id
				mysession.put("username", t.getUsername());
				// 判断是否为管理员，如果是则跳转到管理员界面
				if (t.getType() == 1){
					transaction.commit();
					session.close();
					System.out.println("管理员登陆");
					return "admin";
				}
			}
			System.out.println(t.getUsername());
			System.out.println(t.getPassword());
		}
		transaction.commit();
		session.close();
		return "success";
	}
	
	public String regist()
	{
		Transaction transaction = session.beginTransaction();
		//获取会话，保存各种状态
		Map httpsession = ActionContext.getContext().getSession();
		String username = user.getUsername();
		String password = user.getPassword();
		System.out.println("this regist");
		System.out.println("username is:"+username);
		System.out.println("password is:"+password);
		int temp =0;//0代表注册不符合规则，1符合
		if(username.length()<6)//用户名长度小于6
		{
			httpsession.put("err","903");
			System.out.println("用户名长度小于6");
		}
		else if(password.length()<6)//密码长度至少6位
		{
			httpsession.put("err","904");
			System.out.println("密码长度小于6");
		}
		else //判定是否拥有非法字符，用户名只能为数字字母下划线
		{	int i;
			for(i=0;i<username.length();i++)//用户名只能为数字字母下划线
			{
				char ch=username.charAt(i);
				if(ch=='_')continue;
				if((ch<'0'||ch>'9') && (ch<'a'||ch>'z')&&(ch<'A'||ch>'Z'))
				{
					httpsession.put("err","803");
					System.out.println("用户名只能为数字字母下划线");
				}
			}
			if(i==username.length())//如果最后一位都判断成功，代表符合规则
				temp=1;
		}//end else
		if(temp==1)//输入的注册表单符合注册规则
		{
			System.out.println("开始查询数据库是否存在同名用户");
			String hql="from User where username='"+username+"'";
			Query query = session.createQuery(hql);
			if(query==null)
			{
				System.out.println("query is null");
			}
			else if(query.list().isEmpty())
			{
				System.out.println("数据库不存在同名用户");
				System.out.println("开始插入注册的用户");
				user.setId(null);
				session.save(user);
				httpsession.put("err", "suc");
				
			}
			else
			{
				System.out.println("数据库存在同名用户");
				httpsession.put("err", "900");
			}
			transaction.commit();//提交事务
			session.close();//关闭session对象	
		}//end if
		return "success";
	}
	public String logout()
	{
		Map mysession = ActionContext.getContext().getSession();
		//清空会话
		mysession.clear();
		return "success";
	}
	
	// 管理员添加用户
	public String adminAddUser(){
			Transaction transaction = session.beginTransaction();
			//获取会话，保存各种状态
			Map httpsession = ActionContext.getContext().getSession();
			// 获取从表单上提交的数据
			String username = user.getUsername();
			String userpass = user.getPassword();
			String hql="from User where username='"+username+"'";
			Query query = session.createQuery(hql);	
			if(query.list().isEmpty())
			{
				System.out.println("数据库不存在同名用户");
				System.out.println("开始插入注册的用户");
				user.setId(null);
				session.save(user);
				httpsession.put("err", "suc");
				transaction.commit();//提交事务
			}
			else
			{
				System.out.println("数据库存在同名用户");
				httpsession.put("err", "900");
			}
			transaction.commit();
			session.close();
			return "addUser";
		}
		
		// 管理员删除用户
	public String adminDelUser(){
			System.out.print("delId is:"+delId);
			
			Transaction transaction = session.beginTransaction();
			
			User temp = (User)session.load(User.class, new Integer(delId));
			session.delete(temp);
			
			transaction.commit();
			session.close();
			return "delUser";
		}
		// 保存需要更新的id
	public String update(){
		//获取会话
		Map mysession = ActionContext.getContext().getSession();
		System.out.print("delId is:"+delId);
		mysession.put("userid",""+delId);//会话中保存用户id
		return "updateinfo";
	}
	@SuppressWarnings("finally")
	public String updateInfo(){
			
			System.out.println("update id:" + updateId);
			Transaction transaction = session.beginTransaction();
			Map httpsession = ActionContext.getContext().getSession();
			
			// 获取从表单上提交的数据
			String username = user.getUsername();
			String email = user.getEmail();
			String phone = user.getPhone();
			System.out.println("updateInfo data:"+username + email + phone);
			// 使用load方法更新数据
			try{
				User temp = (User)session.load(User.class, new Integer(updateId));
				temp.setName(username);
				temp.setEmail(email);
				temp.setPhone(phone);
				// 设置成功状态码
				httpsession.put("err", "updateSuccess");
			}catch(Exception e){
				e.printStackTrace();
				// 设置失败状态码
				httpsession.put("err", "updateError");
			}finally {
				transaction.commit();
				session.close();
				return "update";
			}
		}
	public String userUpdateInfo()
	{
		System.out.println("用户更新处理函数");
		if(uinfo_sub!=null)
			System.out.println("用户单机了提交按钮");
		//如果用户超时就是停留一个页面很久session中保存的userid过期了
		String userid = (String) httpSession.get("userid");
		if(userid==null)
		{
			httpSession.put("err","1");
			return "success";
		}
		User temp =(User) session.get(User.class,new Integer(userid));
		//temp.setUsername(user.getUsername());不允许修改用户名
		Transaction ts = session.beginTransaction();
		temp.setPassword(user.getPassword());
		temp.setPhone(user.getPhone());
		temp.setName(user.getName());
		temp.setEmail(user.getEmail());
		//session.saveOrUpdate(temp);
		ts.commit();
		//将更新的用户放到会话中，这句千万不能少，不然前台更新不了
		httpSession.put("user",temp);
		session.close();
		System.out.println("修改后的参数如下");
		System.out.println(user.getUsername());
		System.out.println(user.getPassword());
		System.out.println(user.getPhone());
		System.out.println(user.getName());
		System.out.println(user.getEmail());
		return "userspace";
	}
	public String getUinfo_sub() {
		return uinfo_sub;
	}
	public void setUinfo_sub(String uinfo_sub) {
		this.uinfo_sub = uinfo_sub;
	}
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.user;
	}
}
