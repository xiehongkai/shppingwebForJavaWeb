package Controller;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.Transaction;

import Po.Indent;
import Po.Product;
import Po.User;
import SessionFactory.HibernateSessionFactory;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ModelDriven;

public class IndentAction implements ModelDriven<Indent>{
	String buy,addcar;//对应表单的buy按钮或者addcar按钮
	String log_order;//对应表单的提交按钮
	String pid;//商品的id可以通过session或者Struts2表单自动注入得到
	int number;//选择的商品数量
	String size;//选择的商品尺寸
	String color;//商品颜色
	Indent indent= new Indent() ;//订单持久化对象自动注入
	String orderId; // 订单id自动注入
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getUpdateId() {
		return updateId;
	}
	public void setUpdateId(String updateId) {
		this.updateId = updateId;
	}

	String updateId; // 订单更新id自动注入
	Session session = HibernateSessionFactory.getSession();
	public String execute()
	{
		System.out.println("IndentAction execute方法执行");
		return "success";
	}
	public String buy()//用户单机了购买或加入购物车
	{
		Map httpsession = ActionContext.getContext().getSession();
		String userid = (String) httpsession.get("userid");
		if(userid==null)//未登录单机了购买或者注册
		{
			System.out.println("未登录单击了购买");
			httpsession.put("err", "1");
			return "nologin";
		}
		else
		{
			if(buy!=null)
			{  
				System.out.println("单机了购买按钮");
				String pid = (String) httpsession.get("pid");
				System.out.println("要购买的商品的id是:"+pid);
				System.out.println("商品数量是:"+number);
				System.out.println("商品尺寸是:"+size);
				System.out.println("商品颜色是:"+color);
				return "success";
			}
			return "err";//浏览器地址栏直接输入就进入错误页面
		}//endif
	}//endbuy function
	public String submitIndent()//用户单计了提交订单按钮
	{
		Map httpsession = ActionContext.getContext().getSession();
		String userid = (String) httpsession.get("userid");
		if(userid==null)//未登录或者页面超时了
		{
			httpsession.put("err", "1");
			return "nologin";
		}
		else if(log_order!=null)//是提交按钮到达这个处理方法
		{
			System.out.println("Indentaction's submitIndent执行");
			System.out.println("到达提交订单处理方法,正在生成订单号");
			Calendar c = Calendar.getInstance();    
			int year = c.get(Calendar.YEAR);    
			int month = c.get(Calendar.MONTH)+1;    
			int date = c.get(Calendar.DATE);       
			int hour = c.get(Calendar.HOUR_OF_DAY);   
			int minute = c.get(Calendar.MINUTE);      
			int second = c.get(Calendar.SECOND);   
			System.out.println(year + "-" + month + "-" + date + " " +hour + ":" +minute + ":" + second);
			System.out.println("");
			//（注意:月份从0开始所以要+1）
			int rear = (int) (Math.random()*10000000);
			String oid =""+year+month+date+rear;
			Date d = new Date();
			System.out.println("生成了订单号:"+oid);
			System.out.println("用户id为:"+userid);
			System.out.println("商品id:"+pid);
			System.out.println("支付付款号后8位:"+indent.getPaymentNumber());
			System.out.println("商品名称:+"+indent.getPname());
			System.out.println("商品尺寸:"+indent.getSize());
			System.out.println("商品颜色:"+indent.getColor());
			System.out.println("商品数量:"+indent.getNumber());
			System.out.println("单价:"+indent.getUprice());
			System.out.println("总价:"+indent.getTprice());
			System.out.println("发货状态:"+indent.getSend());
			System.out.println("确认收货状态:"+indent.getConfirm());
			System.out.println("订单状态:"+indent.getStatus());
			System.out.println("订单创建时间:"+indent.getCdate());
			System.out.println("订单完成时间:(第一次应该为空)"+indent.getFdate());
			System.out.println("收货人:"+indent.getRname());
			System.out.println("电话号码:"+indent.getPhonenumber());
			System.out.println("收货地址:"+indent.getAddress());
			
			indent.setOid(oid);
			User user =new User();
			user.setId(Integer.valueOf(userid));
			indent.setUser(user);
			Product product = new Product();
			product.setId(Integer.valueOf(pid));
			indent.setProduct(product);
			Session session =HibernateSessionFactory.getSession();
			//将商品对象保存到订单中
			httpsession.put("indent", indent);
			Transaction ts = session.beginTransaction();
			session.save(indent);
			ts.commit();
			session.close();
			System.out.println("插入订单成功");
			return "buysuccess";
		}
		else //地址栏直接输入就进入404页面
			return "err";//
		
	}
	
	// 保存orderId
		public String orderStatus(){
			
			Map mysession = ActionContext.getContext().getSession();
			System.out.print("orderId is:"+orderId);
			mysession.put("orderId",""+orderId);//会话中保存用户id
			return "orderInfo";
		}
		
		
		// 更新订单状态
		public String orderInfo(){
			System.out.println("update id:" + updateId);
			Transaction transaction = session.beginTransaction();
			Map httpsession = ActionContext.getContext().getSession();
			
			// 获取表单提交上来的数据
			String confirm = indent.getConfirm();
			String send = indent.getSend();
			System.out.println("send is:"+send+"confirm is"+confirm);
			
			try {
				Indent temp = (Indent)session.load(Indent.class, updateId);
				System.out.println("开始更新数据");
				if(send.equals("已发货") && confirm.equals("已收货")){
					temp.setStatus("已完成");
				}
				temp.setConfirm(confirm);
				temp.setSend(send);
				httpsession.put("err", "orUpdateSu");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				httpsession.put("err", "orUpdateEr");
				transaction.rollback();
			}
			transaction.commit();
			session.close();
			return "orderStatus";
		}
		
		public String delOrder(){
			
			System.out.println("orderId is:"+orderId);
			
			Transaction transaction = session.beginTransaction();
			try {
				Indent temp = (Indent)session.load(Indent.class, orderId);
				session.delete(temp);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				transaction.rollback();
			}
			transaction.commit();
			session.close();
			return "delOrder";
		}
		
	
	
	public String getBuy() {
		return buy;
	}
	public void setBuy(String buy) {
		this.buy = buy;
	}
	public String getAddcar() {
		return addcar;
	}
	public void setAddcar(String addcar) {
		this.addcar = addcar;
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getLog_order() {
		return log_order;
	}
	public void setLog_order(String log_order) {
		this.log_order = log_order;
	}
	
	public Indent getModel() {
		// TODO Auto-generated method stub
		return indent;
	}
}
