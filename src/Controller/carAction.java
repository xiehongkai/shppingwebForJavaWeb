package Controller;

import java.util.Map;

import org.hibernate.Session;
import org.hibernate.Transaction;

import Po.CarProduct;
import Po.Product;
import Po.User;
import SessionFactory.HibernateSessionFactory;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ModelDriven;

public class carAction implements ModelDriven<CarProduct>{
	String addcar;//加入购物车按钮
	Session session = HibernateSessionFactory.getSession();
	CarProduct carproduct=new CarProduct();//自动注入对象
	private String pid;//商品id
	public String addcar()//用户单机了加入购物车
	{
		Map httpsession = ActionContext.getContext().getSession();
		String userid = (String) httpsession.get("userid");
		if(userid==null)//未登录单机了购买或者注册或者页面超时
		{
			System.out.println("未登录单击了加入了购物车");
			httpsession.put("err", "1");
			return "nologin";
		}
		else if(addcar!=null)//单机了购物车进入了这里,不是地址栏直接输入的
		{
			
			System.out.println("单机了加入了购物车按钮");
			System.out.println("要加入购物车的商品的id是:"+pid);
			System.out.println("商品数量是:"+carproduct.getNumber());
			System.out.println("商品单价是:"+carproduct.getUprice());
			System.out.println("商品尺寸是:"+carproduct.getSize());
			System.out.println("商品颜色是:"+carproduct.getColor());
			Transaction ts = session.beginTransaction();
			User user =new User();
			user.setId(new Integer(userid));
			carproduct.setUser(user);
			Product product = new Product();
			product.setId(new Integer(pid));
			carproduct.setProduct(product);
			session.saveOrUpdate(carproduct);
			ts.commit();
			session.close();
			//将对象放到会话中给加入购物车页面查询信息
			httpsession.put("carproduct", carproduct);
			System.out.println("加入购物车成功");
			return "success";
		}
		else return "err";
	}
	public String getAddcar() {
		return addcar;
	}
	public void setAddcar(String addcar) {
		this.addcar = addcar;
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public CarProduct getModel() {
		// TODO Auto-generated method stub
		return this.carproduct;
	}
	
}
