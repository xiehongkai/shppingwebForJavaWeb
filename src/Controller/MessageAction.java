package Controller;

import java.util.Map;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ModelDriven;

import Po.Message;
import Po.Product;
import SessionFactory.HibernateSessionFactory;

public class MessageAction implements ModelDriven<Message>{
	private Message message = new Message();
	Session session = HibernateSessionFactory.getSession();
	
	String updateId; // 自动注入
	String delId;
	public String sendMsg(){
		
		Transaction transaction = session.beginTransaction();
		//获取会话，保存各种状态
		Map httpsession = ActionContext.getContext().getSession();
		// 获取从表单上提交的数据
		String name = message.getName();
		String email = message.getEmail();
		String address = message.getAddress();
		String msg = message.getMsg();
		System.out.println("Name is:"+name+"\t email is:" + email + " \taddress is:"+address+"\t msg is:"+msg);
		// 保存到数据库
		session.save(message);
		// 发送成功状态码
		httpsession.put("err", "sendSu");
		transaction.commit();
		session.close();
		return "sendMsg";
	}
	
	public String update(){
		System.out.println("updateId is:" + updateId);
		Map mysession = ActionContext.getContext().getSession();
		mysession.put("messageId",""+updateId);//会话中保存用户id
		return "updateInfo";
	}
	
	
	public String updateInfo(){
		System.out.println("update id:" + updateId);
		Transaction transaction = session.beginTransaction();
		Map httpsession = ActionContext.getContext().getSession();
		
		// 获取表单提交的数据
		boolean messageStatus = message.isStatus();
		System.out.println("messageStatus is:" + messageStatus);
		
		try {
			Message temp = (Message)session.load(Message.class, new Integer(updateId));
			temp.setStatus(messageStatus);
			httpsession.put("err", "msgSuccess");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			// 设置失败状态码
			httpsession.put("err", "msgError");
			// 回滚
			transaction.rollback();
		}
		
		transaction.commit();
		session.close();
		return "update";
	}
	
	public String delMessage(){
		System.out.println("delId is:"+delId);
		Transaction transaction = session.beginTransaction();
		Map httpsession = ActionContext.getContext().getSession();
		
		// 删除留言
		Message temp = (Message)session.load(Message.class, new Integer(delId));
		session.delete(temp);
		
		transaction.commit();
		session.close();
		return "delMsg";
	}
	
	public String getDelId() {
		return delId;
	}

	public void setDelId(String delId) {
		this.delId = delId;
	}

	public String getUpdateId() {
		return updateId;
	}

	public void setUpdateId(String updateId) {
		this.updateId = updateId;
	}

	public Message getModel() {
		// TODO Auto-generated method stub
		return this.message;
	}
}
